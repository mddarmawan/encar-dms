var firebaseDB = firebase.database();

//PARSING SPK & DISKON SALES
$.ajax({
	type: "GET",
	url: "/api/firebase/sales/"
}).done(function(data){
	if (data){
		for(var i in data){
			var sales = data[i];
			parseSPKSales(sales.sales_salesUid);			
			parseDiskonSales(sales.sales_salesUid);		 
		}
	}
}); 

function parseSPKSales(uid){
	var db = firebase.database().ref('sales/' + uid + '/spk/');
	db.on('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var item = childSnapshot.val();
			item['spk_id'] = childSnapshot.key;
			item['_token'] = $('meta[name="csrf-token"]').attr('content');

			if (item.spk_status != 99){
				$.ajax({
					type: "POST",
					url : "/api/firebase/spk",
					data: item
				}).done(function(result){
					if (result){
						console.log(result);
						//db.child(childSnapshot.key).remove();
					}
				}); 
			}
		});
	});
}

function parseDiskonSales(uid){
	var db = firebaseDB.ref('sales/' + uid + '/diskon/');
	db.on('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var item = childSnapshot.val();
			item['_token'] = $('meta[name="csrf-token"]').attr('content');
			
			if (item.diskon_status != 9){
				$.ajax({
					type: "POST",
					url: "/api/firebase/diskon/",
					data: item
				}).done(function(result){
					if (result){
						console.log(result);
						//db.child(childSnapshot.key).remove();
					}
				}); 
			}
		});
	});
}