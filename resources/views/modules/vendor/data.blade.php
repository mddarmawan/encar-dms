@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pembelian</small>
            Vendor Kendaraan
        </h6>
    </div>

<div class="wrapper">
    <div id="dataVendor">

    </div>  
</div>

<script>
$(function() {

    var db_vendor = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/vendor')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/vendor')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/vendor')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/vendor')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };

    $("#dataVendor").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_vendor,
 
        fields: [
            { name: "vendor_nama", title:"Nama Vendor", type: "text", width: 150, validate: "required" },
            { name: "vendor_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "vendor_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "vendor_kodepos", title:"Kode Pos", type: "number", width: 120},
            { name: "vendor_notelp", title:"Telepon", type: "number", width: 100, validate: "required" },
            { name: "vendor_email", title:"Email", type: "text", width: 100 },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

@endsection