@extends('layout')

@section('content')

<form id="import_form" style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="/api/pembelian/import" class="form-horizontal" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input id="import_file" type="file" name="import_file" style="display:none"/>
</form>

<div class="content-header">
    <h6 class="left">
        <small>Pembelian</small>
        Transaksi Pembelian Kendaraan
    </h6>
    <ul class="header-tools right">
       <li><a id="import" class="chip" style="cursor: pointer;"><i class="fa fa-plus"></i> Import File</a></li>
       <li><a href="#tambah" class="chip tambah"><i class="fa fa-plus"></i> Pembelian Baru</a></li>
    </ul>
</div>

<div class="wrapper">
    <div id="data">

    </div>  
</div>

<div id="tambah" class="modal modal-fixed-footer" style="width:800px; height: auto;">
    <form method="POST" action="{{url('/leashing')}}">
        <h6 class="modal-title blue-grey darken-1">
            <span id="form_title">Pembelian Baru</span>
            <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
        </h6>

        <div class="modal-content" style="padding:10px;position: relative;">
            @include("modules.pembelian.tambah")
        </div>
          <div style="padding:10px;text-align:right;background:#f5f5f5">
            <a class="waves-effect waves-light btn save"><i class="material-icons left">save</i> <span id="btn_save">Konfirmasi Pembelian</span></a>
        </div>
     </form>
</div>


<script>
$("#import").click(function() {
    $("#import_file").trigger("click");
});

$("input:file").change(function() {
    $("#import_form").submit();
    loadData();
});

function loadData() {

    var db = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/pembelian')}}",
                    data: filter
                });
            },


        deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/pembelian')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            }
    };

    $("#data").jsGrid({
        height: "100%",
        width: "100%",
        editing: false,
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "trk_ref", title:"Referensi", type: "text", width: 80, align:"center" },
            { name: "trk_invoice", title:"Vendor Invoice", type: "text", width: 80, align:"center" },
            { name: "trk_tgl", title:"Tgl. Invoice", type: "text", width: 70, align:"center" },
            { name: "trk_vendor", title:"Vendor", type: "text", width: 120},
            { name: "trk_dh", title:"NO DH", type: "text", width: 70, align:"center" },
            { name: "trk_rrn", title:"RRN", type: "text", width: 120 },
            { name: "trk_variant", title:"Type/Varian", type: "text", width: 120},
            { name: "trk_warna", title:"Warna", type: "text", width: 100},
            { name: "trk_harga", title:"Harga (Rp)", type: "number", width: 100, align:"right"},
            { type: "control",  width:50, align:"center", editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    
                    var $customButton = $('<input class="jsgrid-button jsgrid-edit-button" type="button" title="Edit" style="float:left">')
                        .click(function(e) {
                            $("#state").val("edit");

                            $("#form_title").html("EDIT: "+ item.trk_ref);
                            $("#btn_save").html("Simpan Perubahan");

                            $("#_id").val(item.trk_id);
                            $("#trk_ref").val(item.trk_ref);
                            $("#trk_invoice").val(item.trk_invoice);
                            $("#trk_tgl").val(item.trk_tgl);
                            $("#trk_tahun").val(item.trk_tahun);
                            $("#trk_dh").val(item.trk_dh);
                            $("#trk_rrn").val(item.trk_rrn);
                            $("#trk_rangka").val(item.trk_rangka);
                            $("#trk_mesin").val(item.trk_mesin);
                            $("#trk_dpp").val(item.trk_dpp);
                            $("#trk_vendor").val(item.trk_vendorid);
                            $("#trk_warna").val(item.trk_warnaid);
                            $("#type").val(item.trk_type);
                            $("#trk_variant").val(item.trk_variantid);

                            $(".modal").modal("open");
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });
 
   
};
loadData();
</script>

@endsection