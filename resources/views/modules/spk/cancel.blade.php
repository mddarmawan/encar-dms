@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Monitoring SPK
		</h6>
		<ul class="header-tools right">
           <li><a href="{{url('spk/no_spk')}}" class="chip ">Pembagian No SPK</a></li>
           <li><a href="{{url('spk')}}" class="chip ">Monitoring SPK</a></li>
			<li><a href="{{url('spk/permintaan_spk')}}" class="chip ">Permintaan SPK</a></li>
			<li><a href="{{url('spk/cancel')}}" class="chip active">Batal SPK</a></li>
			<li><a href="{{url('spk/permintaan_do')}}" class="chip">Permintaan DO</a></li>
		</ul>
	</div>		

<div class="wrapper">
	<div class="nav-wrapper">
		<div class="nav-left">
			<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
			<span class="bold" style="font-size: 13px"> Periode :</span>
			<input type="text" class="datepicker" id=""  required="" /><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			<span class="bold" style="font-size: 13px">S/D</span>
			<input type="text" class="datepicker" id=""  required="" /><span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
			<a class="waves-effect waves-light btn cyan" id="filter">Filter </a>

			<div style="float: right; display: inline-block;">
				<i style=
				"margin-right: 5px" class="fa fa-file-excel-o" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px;  margin-right: 10px">Export to Excel</span>
			
			
				<i style="margin-right: 5px" class="fa fa-refresh" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px">Refresh</span>
		
			</div>
		</div>

		

	</div>

	<div id="dataSPK">

	</div>	


</div>

@include("modules.spk.detail_cancel")

<script>
	var total_bayar=0;
	var piutang = 0;
	var total_aksesoris = 0;
	var total_diskon = 0;
	var total_leasing = 0;
	var hpp=0;
	var bbn=0;
	var off=0;
	var dpp=0;
	var ppn=0;
	function detail(e){
		var id = $(e).attr("data-id");
		$("#detail a[href='#pemesan").trigger("click");
		$("#spk_id").html(id);
		$("#msg").html('');

		$.ajax({
            type: "GET",
            url: "{{url('api/spk/')}}/"+id
        }).done(function(json) {
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		 	var diskon = json.diskon;
		 	var aksesoris = json.aksesoris;
		 	var leasing = json.leasing;
		 	var faktur = json.faktur;
		 	var bayar = json.bayar;
		 	var d_leasing = json.d_leasing;
		 	var d_asuransi = json.d_asuransi;
		 	var no_faktur = json.no_faktur;
		 	total_bayar=0;
		 	piutang = 0;
			total_aksesoris = 0;
			total_diskon = 0;
			total_leasing = 0;
		 	hpp=0;
		 	bbn = pemesan.variant_bbn;
		 	off=pemesan.variant_on - bbn;
		 	if (pemesan.spk_bbn != "" && pemesan.spk_bbn != null){
		 		off=pemesan.spk_dpp;
			 	bbn=pemesan.spk_bbn;
			 }

			 if(pemesan.spk_status==10){

			 	$("#cancel_button").hide();
			 }

			$(".cetak span").html("Request Cetak");
		 	if(faktur !=null){
		 		$("#cetak").attr("disabled","true");
		 		$("#spk_faktur").val(faktur.spkf_id);
		 		$("#spk_tanggal").val(date_format(faktur.spkf_tgl));
				if (faktur.spkf_cetak == 0){
					$(".cetak").attr("disabled","true");
				}else if (faktur.spkf_cetak == 1){
					$(".cetak span").html("Cetak Faktur");
					$(".cetak").removeAttr("disabled");
				}
		 	}else{
		 		$("#spk_faktur").val(no_faktur);
		 		$("#cetak").removeAttr("disabled");
		 	}


		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );

		 	$("#pel_nama").html(pemesan.spk_pel_nama);
		 	$("#pel_identitas").html(pemesan.spk_pel_identitas);
		 	$("#pel_alamat").html(pemesan.spk_pel_alamat);
		 	$("#pel_pos").html(pemesan.spk_pel_pos);
		 	$("#pel_telp").html(pemesan.spk_pel_telp);
		 	$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
		 	$("#pel_email").html(pemesan.spk_pel_email);
		 	$("#pel_kategori").html(pemesan.spk_pel_kategori);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);
		 	$("#spk_ket_cancel").html(pemesan.spk_ket_cancel);
		 	$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
		 	$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
		 	$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_identitas").html(pemesan.spk_stnk_identitas);

		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	if (kendaraan!=null){
				$("#kendaraan .no-data").removeClass("show");
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
				$("#kendaraan .no-data").addClass("show");
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){		 	
		 		hpp = off+bbn;
		 		$("#spk_kategori").html("ON THE ROAD");
		 		$("#on").show();
		 		$("#bbn").show();
		 		$("#off").attr("readonly","");
		 		$("#on").val(number_format(pemesan.variant_on));
		 	}else{
		 		hpp = off;
		 		bbn=0;
		 		$("#on").hide();
		 		$("#bbn").hide();
		 		$("#off").removeAttr("readonly");
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}
		 	$("#bbn").val(number_format(bbn));
		 	$("#off").val(number_format(off));

		 	if (diskon!=null){
				$("#diskon .no-data").removeClass("show");
			 	$("#diskon_cashback").html(number_format(diskon.spkd_cashback));
			 	$("#diskon_komisi").html(number_format(diskon.spkd_komisi));
			 	$("#komisi").val(number_format(diskon.spkd_komisi));
			 	$('#aksesoris').html('');
			 	for(var i in aksesoris){
			 		var no = parseInt(i)+1;
			 		var obj = aksesoris[i];
			 		total_aksesoris += obj.spka_harga;
			 		$('#aksesoris').append('<tr class="jsgrid-row"><td class="jsgrid-cell" width="180px" style="padding-left:30px!important">'+ (no) +'. '+ obj.spka_kode +' - '+ obj. spka_nama+'</td><td class="bold jsgrid-cell text-right">'+number_format(obj.spka_harga)+'</td></tr>');

			 	}
			 	total_diskon = diskon.spkd_cashback +  total_aksesoris;
			 	$("#diskon_ket").html(diskon.spkd_ket);
			 	$("#diskon_total").html(number_format(total_diskon));
			 	$("#potongan").val(number_format(total_diskon));

		 		dpp = Math.round((off - total_diskon)/1.1);
		 		ppn = Math.round(dpp * 0.1);
		 		hpp -= total_diskon;
			 	$("#trk_dpp").val(number_format(dpp));
			 	$("#trk_ppn").val(number_format(ppn));
			}else{
				$("#diskon .no-data").addClass("show");
				$("#diskon_cashback").html('');
			 	$("#diskon_komisi").html('');
			 	$("#komisi").val('');
			 	$('#aksesoris').html('');
			 	$("#diskon_ket").html('');
			 	$("#diskon_total").html('');
			 	$("#potongan").val('');

			 	$("#hpp").val("");
			 	$("#trk_dpp").val("");
			 	$("#trk_ppn").val("");
			}
			$("#hpp").val(number_format(hpp));

			$("#tabel_pembayaran").html('');
		 	if (bayar!=null){
		 		for(var i in bayar){
		 			var item = bayar[i];
		 			$("#tabel_pembayaran").append('<tr class="jsgrid-row"><td class="jsgrid-cell">'+date_format(item.spkp_tgl)+'</td><td class="jsgrid-cell text-right">'+number_format(item.spkp_jumlah)+'</td></tr>');
		 			total_bayar +=item.spkp_jumlah;
		 		}
		 	}
		 	$("#total_bayar").val(number_format(total_bayar));
		 	piutang = hpp - total_bayar;

		 	if (pemesan.spk_pembayaran==0){
		 		$('#spk_metode').html("CASH");
		 		$('.leasing').hide();
		 	}else{
		 		$('#spk_metode').html("CREDIT");
		 		$('.leasing').show();		 		
		 	}

		 	$('#spk_waktu').val(pemesan.spk_waktu);
		 	var ls = pemesan.leasing_id;
		 	var as = '';
		 	$('#spk_leasing').val(pemesan.leasing_nama);
		 	if (leasing!=null){
		 		ls = leasing.spkl_leasing;
		 		as = leasing.spkl_jenis_asuransi;
		 		$('#spk_waktu').val(leasing.spkl_waktu);
		 		$('#spk_angsuran').val(number_format(leasing.spkl_angsuran));
		 		$('#spk_dp').val(number_format(leasing.spkl_dp));
		 		$('#spk_droping').val(number_format(leasing.spkl_droping));
		 		total_leasing = toInt(leasing.spkl_dp) + toInt(leasing.spkl_droping);
		 		$('#spk_tleasing').val(number_format(total_leasing));
		 		piutang = piutang - leasing.spkl_droping; 
		 	}else{
		 		$('#spk_waktu').val('');
		 		$('#spk_angsuran').val('');
		 		$('#spk_dp').val('');
		 		$('#spk_droping').val('');
		 		$('#spk_tleasing').val('');
		 	}


		 	$('#spk_leasing').html('');
		 	for(var i in d_leasing){
		 		var item = d_leasing[i];
		 		$('#spk_leasing').append('<option value="'+item.leasing_id+'" '+selected(item.leasing_id,ls)+'>'+item.leasing_nama+'</option>');
		 	}

		 	$('#spk_asuransi').html('');
		 	for(var i in d_asuransi){
		 		var item = d_asuransi[i];
		 		$('#spk_asuransi').append('<option value="'+item.ajenis_id+'" '+selected(item.ajenis_id,as)+'>'+item.ajenis_nama+'</option>');
		 	}
		 	$("#piutang").val(number_format(piutang));

		 	$(".cetak").unbind().click(function(){
				if (faktur ==null){
					set_request(pemesan);
				}else{
					if (faktur.spkf_cetak == null){
						set_request(pemesan);
					}else{
						$("#cetak").modal("open");
					}
				}
		 	});
			

		});
	};
	
	function set_request(pemesan){
		var faktur_id = $("#spk_faktur").val();
		$("#faktur_no").html(faktur_id);
		$(".app").click(function(){
			var value = $(this).val();
							
			if (value==1){
				var data = {
					"spkf_id":faktur_id,
					"spkf_spk":pemesan.spk_id,
					_token:'{{csrf_token()}}'
				};
								
				$.ajax({
					type: "POST",
					dataType:"json",
					url: "{{url('/api/spk/reqfaktur')}}",
					data: data
				}).fail(function(response) {
					alert("ERR-42 Request Cetak Gagal Dikirim !");
				}).done(function(data){
					
					if (!data.result){
						alert(data.msg);
					}else{
						alert("Permintaan Cetak Faktur ["+faktur+"] telah dikirim, Silahkan menunggu Persetujuan untuk melanjutkan proses Cetak Faktur.");
						$(".cetak").attr("disabled","true");
					}
				});
			}
		});
		$("#request").modal("open");
	}
	
	
	$("#bbn").change(function(){
		var otr = toInt(string_format($("#on").val()));
		var off = otr - toInt($(this).val());
		var potongan = toInt(string_format($("#potongan").val()));
		var dpp = Math.round((off - potongan) / 1.1);
		var ppn = Math.round(dpp * 0.1);
		$("#off").val(number_format(off));
		$("#trk_dpp").val(number_format(dpp));
		$("#trk_ppn").val(number_format(ppn));
	});

	$('#bbn').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('on').focus();
		}
	});

	$("#spk_dp").change(function(){
		$("#spk_tleasing").val(number_format(toInt($(this).val())+toInt($("#spk_droping").val())));	
	});

	$('#spk_dp').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_droping').focus();
		}
	});

	$("#spk_droping").change(function(){
		piutang = (hpp -  total_bayar) - toInt($(this).val());
		$("#piutang").val(number_format(piutang));
		$("#spk_tleasing").val(number_format(toInt($(this).val())+toInt($("#spk_dp").val())));
	});

	$('#spk_droping').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_tleasing').focus();
		}
	});
	$('#spk_angsuran').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_dp').focus();
		}
	});
	$('#spk_waktu').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_angsuran').focus();
		}
	});

	$("#cancel").click(function(){
		$("#cancel_request").modal("open");
	});

	$("#cancel_button").click(function(){

			var data = {
			"spk_id":$("#spk_id").html(),
            _token:'{{csrf_token()}}'
			};
			$.ajax({
	            type: "POST",
	            dataType:"json",
	            url: "{{url('/api/spk/cancel')}}",
	            data: data
	        }).fail(function(response) {
	            alert("ERR-42 Data Gagal Dibatalkan !");
	        }).done(function(data){
	        	if (!data.result){
	        		alert(data.msg);
	        	}else{
		        	$('#msg').html('Data Dibatalkan !');
	        	}
	        });
				$("#cancel_button").hide();
			loadData();


	});



	$("#save").click(function(){
		
		var data = {
			"spk_id":$("#spk_id").html(),
			"spk_dpp":toInt($("#off").val()),
			"spk_bbn":toInt($("#bbn").val()),
            _token:'{{csrf_token()}}'
		};
		$.ajax({
            type: "POST",
            dataType:"json",
            url: "{{url('/api/spk/update')}}",
            data: data
        }).fail(function(response) {
            alert("ERR-42 Data Gagal Disimpan !");
        }).done(function(data){
        	if (!data.result){
        		alert(data.msg);
        	}else{
        		if ($("#spk_pembayaran").val('CREDIT')){
	        		var data = {
						"spkl_spk":$("#spk_id").html(),
						"spkl_leasing":$("#spk_leasing").val(),
						"spkl_asuransi":$("#spk_asuransi").val(),
						"spkl_angsuran":toInt($("#spk_angsuran").val()),
						"spkl_waktu":$("#spk_waktu").val().toUpperCase(),
						"spkl_dp":toInt($("#spk_dp").val()),
						"spkl_droping":toInt($("#spk_droping").val()),
			            _token:'{{csrf_token()}}'
					};
					$.ajax({
			            type: "POST",
			            dataType:"json",
			            url: "{{url('/api/spk/leasing')}}",
			            data: data
			        }).fail(function(response) {
			            alert("ERR-42 Data Gagal Disimpan !");
			        }).done(function(data){
			        	if (!data.result){
			        		alert(data.msg);
			        	}else{
			        		$('#msg').html('Tersimpan !');
			        	}
			        });
			    }else{
	        		$('#msg').html('Tersimpan !');
	        	}
        	}
        });
	});

	$("#terbitkan").click(function(){
		if (piutang!=0){
			alert("Piutang belum 0 (NOL)\nFaktur Penjualan atas SPK " + $("#spk_id").html()+" belum bisa diterbitkan!")
		}else{
			var data = {
				"spk_id":$("#spk_id").html(),
				"spkf_id":$("#spk_faktur").val(),
				_token:'{{csrf_token()}}'
			};
			$.ajax({
				type: "POST",
				dataType:"json",
				url: "{{url('/api/spk/posting')}}",
				data: data
			}).fail(function(response) {
				alert("ERR-42 Data Gagal Diposting !");
			}).done(function(data){
				if (!data.result){
					alert(data.msg);
				}else{
					loadData();
					$("#terbitkan").modal("close");
				}
			});
		}
	})

	$("#cetak").click(function(){

	})


function loadData() {


	var db_spk = {
        loadData: function(filter) {
			return $.ajax({
                type: "GET",
                url: "{{url('api/spk/cancel')}}",
                data: filter
            });
        },
        onDataLoaded: function(args) {

        }
    };

    db_spk.status = [
    		{
            	"status_id": "",
            	"status_nama": "",           
        	},
    		
    		{
            	"status_id": "10",
            	"status_nama": "Batal",           
        	},

    		{
            	"status_id": "11",
            	"status_nama": "Req Batal",           
        	},
        ];
	db_spk.harga = [
    		{
            	"harga_id": "",
            	"harga_nama": "",           
        	},
    		{
            	"harga_id": "1",
            	"harga_nama": "OFF-TR",           
        	},
    		{
            	"harga_id": "0",
            	"harga_nama": "ON-TR",           
        	},
    		
        ];
	

    $("#dataSPK").jsGrid({
        height: "95%",
        width: "100%",
 
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_spk,
 
        fields: [
             { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
           // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
            { name: "spk_variant", title:"Varian", type: "text", width: 150 },
            { name: "spk_warna", title:"Warna", type: "text", width: 120, align:"center"},

            { name: "spk_sales", title:"Sales", type: "text", width: 120},

            { name: "spk_team", title:"Team", type: "text", width: 120, align:"center" },
			{ name: "spk_kendaraan_harga", title:"Jenis Harga", type: "select", width: 100, items: db_spk.harga, valueField: "harga_id", textField: "harga_nama", align:"center" },

            { name: "spk_pembayaran", title:"Pembayaran", type: "text", width: 100, align:"right" },
            { name: "spk_via", title:"Via", type: "text", width: 70, align:"center" },
            { name: "spk_pel_kota", title:"Kota", type: "text", width: 100, align:"center" },
            { name: "spk_status", title:"Status", type: "select", width: 100, items: db_spk.status, valueField: "status_id", textField: "status_nama", align:"center" },
            { name: "spk_ket_cancel", title:"Catatan Pembatalan", type: "text", width: 180 },
            { name: "detail", title:"", width:50, align:"center" }
        ]
    });
}
loadData();

</script>
@endsection