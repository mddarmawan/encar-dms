<div class="row" style="margin:0">
    <input type="hidden" id="state" value="add"/>
    <input type="hidden" id="_id" value=""/>
    <table class="info payment" style="border:0;margin-top:0">
        <tr>
            <td width="140px">No SPK Terakhir</td>
            <td width="10px">:</td>
            <td class="bold"><div id="spkNo_min"></div></td>
        </tr>
        <tr>
            <td width="140px">Jumlah No SPK</td>
            <td width="10px">:</td>
            <td class="bold"><input type="number" id="spkNo_max"/></td>
        </tr>
        <tr>
            <td width="140px">Sales</td>
            <td width="10px">:</td>
            <td class="bold">
                <select id="spkNo_sales">

                </select>
            </td>
        </tr>
    </table>
</div>           

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>

<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
    authDomain: "dealer-ms.firebaseapp.com",
    databaseURL: "https://dealer-ms.firebaseio.com",
    projectId: "dealer-ms",
    storageBucket: "dealer-ms.appspot.com",
    messagingSenderId: "950788435795"
  };
  firebase.initializeApp(config);
  firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
      console.log(error.message);
    });
</script>

<script>
    $(function() {
        function lastNo() {
            $.ajax({
                type: "GET",
                url: "{{url('api/pengaturan/lastno')}}"
            }).done(function(data) {
                $("#spkNo_min").html(data.spk_id);
            });
        }

        function setSales(){
            $.ajax({
                type: "GET",
                url: "{{url('api/karyawan/sales')}}"
            }).done(function(data) {
                $("#spkNo_sales").html("");
                jQuery.each(data, function(i, item) {
                    $("#spkNo_sales").append("<option value='"+ item.sales_id +"'>"+ item.sales_karyawan +"</option>");
                });
            });
        }
        
        function clear(){
            $("#spkNo_min").html("");
            $("#spkNo_max").val("");
          
            setSales();
        }

    $(".tambah").click(function(){
        $("#form_title").html("Tambah No");
        $("#btn_save").html("Tambah");
        clear();
        $("#state").val("add");
        lastNo();
    });

    clear();
});
</script>