<div id="pemesan">
	<div class="row" style="margin:0">
	  	<div class="col s12 m6">
	  		<table class="info payment" style="border:0;margin-top:10px">
				<tr>
					<td width="100px">Kategori</td>
					<td width="10px">:</td>
					<td class="bold">
						<select id="kategori">
							<option value="PERSONAL">PERSONAL</option>
							<option value="PERUSAHAAN">PERUSAHAAN</option>
						</select>
					</td>
				</tr>
			  	<tr>
					<td width="140px">No SPK<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_id" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Nama Pemesan<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_nama" required="" /></td>
				</tr>
				<tr>
					<td width="140px">No. KTP/KIMS<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_identitas" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Tanggal Lahir<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" class="datepicker" id="spk_pel_lahir" value="<?php echo date("d/m/Y") ?>" required="" /></td>
				</tr>
				<tr>
					<td width="140px" style="vertical-align:top ">Alamat<span style="color: red">(*)</span></td>
					<td width="10px" style="vertical-align:top ">:</td>
					<td class="bold"> <textarea id="spk_pel_alamat" class=""></textarea></td>
				</tr>
				
			</table>
	  	</div>	  			
	  	<div class="col s12 m6"  style="border-left:1px solid #ddd">
	  		<table class="info payment" style="border:0;margin-top:10px">
	  			<tr>
					<td width="140px">Kode Pos</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_pos" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Telp.</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_telp" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Ponsel<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_ponsel"  /></td>
				</tr>
				<tr>
					<td width="140px">E-mail</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_email" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Kota<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_pel_kota" required="" /></td>
				</tr>
				
				<tr class="spk_kategori_column">
						<td width="140px">NPWP<span style="color: red">(*)</span></td>
						<td width="10px">:</td>
						<td class="bold"><input type="text" id="spk_npwp" required="" /></td>
				</tr>
					
			
				<tr class="spk_kategori_column">
					<td width="100px">Kategori</td>
					<td width="10px">:</td>
					<td class="bold">
						<select id="pajak">
							<option value="0">TIDAK DI MINTA</option>
							<option value="1">DI MINTA</option>
						</select>
					</td>
				</tr>
	  		</table>
	  		<div class="spk_kategori_column">
	  			<p style="font-size: 13px">Nama dan Jabatan Contact Person Customer Corporate/Fleet:<span style="color: red">(*)</span><input style="width: 100%" type="text" id="spk_npwp" required="" /></p>
	  		</div>
	  	</div>
	 </div>
</div>
<div id="stnk">
	<div class="row" style="margin:0">
		<div class="col m6"  style="border-right:1px solid #ddd;">
			<table class="info payment" style="border:0;margin-top:0">
				<tr>
				 	<input type="checkbox" class="material" id="informasi" />
				 	<label for="informasi" style="color:#000">Informasi STNK sama dengan Pemesan</label>
				</tr>
				<hr/>
				<tr>
					<td width="140px">Faktur STNK a/n<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_nama" required="" /></td>
				</tr>
				<tr>
					<td width="140px">No. KTP/KIMS<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_ktp" required="" /></td>
				</tr>
				<tr>
					<td width="140px" style="vertical-align:top ">Alamat KTP/KIMS<span style="color: red">(*)</span></td>
					<td width="10px" style="vertical-align:top ">:</td>
					<td class="bold"> <textarea id="spk_stnk_alamat" class=""></textarea></td>
				</tr>
				<tr>
					<td width="140px">Kode Pos</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_pos" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Telp.</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_telp" required="" /></td>
				</tr>
				<tr>
					<td width="140px">Ponsel</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_ponsel"  /></td>
				</tr>
				<tr>
					<td width="140px">E-mail</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_email" required="" /></td>
				</tr>
		</table>
		</div>	  		
		<div class="col m6">
		   <table class="info payment" style="border:0;margin-top:0">
				<tr>
				 	<input type="checkbox" class="material " id="alamat" />
				 	<label for="alamat" style="color:#000">Alamat tempat tinggal sama dengan alamat ktp</label>
				</tr>
				<hr/>
				<tr>
					<td width="140px" style="vertical-align:top ">Alamat Tempat Tinggal<span style="color: red">(*)</span></td>
					<td width="10px" style="vertical-align:top ">:</td>
					<td class="bold"> <textarea id="spk_stnk_alamatd" class=""></textarea></td>
				</tr>
				<tr>
					<td width="140px">Kode Pos</td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="spk_stnk_posd" required="" /></td>
				</tr>
		</table>
		</div>
 	</div>
</div>	
<div id="kendaraan" style="padding:0px">
	<div class="row" style="margin:0px; padding:0px;">
	  	<div class="col m6"  style="border-right:1px solid #ddd">
			<h6 style="padding:0 ;font-weight:bold">Kendaraan</h6>
			<hr>
	  		<table class="info payment" style="border:0;margin-top:0">
	  		<tr>
				<td width="140px">Type</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="type">

					</select>
				</td>
			</tr>
		  	<tr>
				<td width="140px">Variant</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spk_kendaraan">

					</select>
				</td>
			</tr>
		  	<tr>
				<td width="140px">Warna</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spk_warna">

					</select>
				</td>
			</tr>
		</table>
	  	</div>
	  	<div class="col m6">
			<h6 style="padding:0 ;font-weight:bold">Diskon</h6>
			<hr>
		  	<table class="info payment" style="border:0;margin-top:0">
		  		<tr>
					<td width="140px">Cashback<span style="color: red">(*)</span></td>
					<td width="10px">:</td>
					<td class="bold"><input type="text" id="cashback"  /></td>
				</tr>
				<tr>
				<td width="140px">Referral</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spkd_referral">

					</select>
				</td>
			</tr>
			  	<tr>
				   <td width="140px" style="vertical-align:top ">Keterangan<span style="color: red">(*)</span></td>
				   <td width="10px" style="vertical-align:top ">:</td>
				   <td class="bold"> <textarea id="keterangan" class=""></textarea></td>
				</tr>
			  
			</table>
	  	</div>
	</div>
</div>
<div id="pembayaran" style="padding:0px;">	  		
	 <div class="row" style="margin:0px; padding:0px;">
	  	<div class="col m6"  style="border-right:1px solid #ddd">
			<h6 style="padding:0 ;font-weight:bold">Kendaraan</h6>
			<hr>
	  		<table class="info payment" style="border:0;margin-top:0">
	  		<tr>
				<td width="100px">Sales</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spk_sales">

					</select>
				</td>
			</tr>
			<tr>
				<td width="100px">Pembayaran</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spk_pembayaran">
						<option value="0">CASH</option>
						<option value="1">CREDIT</option>
					</select>
				</td>
			</tr>
			<tr id="spk_leasing_column">
				<td width="100px">Leasing</td>
				<td width="10px">:</td>
				<td class="bold">
					<select id="spk_leasing">
					</select>
				</td>
			 
			</tr>
			
		</table>
	  	</div>
	</div>
</div>
<script>
	$(function() {

		$("#spk_leasing_column").hide();
		$("#spk_pembayaran").on('change', function(){
			var optionSelected = $("option:selected", this);
			var valueSelected = this.value;
			if (valueSelected == '0') {
				$("#spk_leasing_column").hide();
			} else {
				$("#spk_leasing_column").show();
			}
		});

		$(".spk_kategori_column").hide();
		$("#kategori").on('change', function(){
			var optionSelected = $("option:selected", this);
			var valueSelected = this.value;
			if (valueSelected == 'PERSONAL') {
				$(".spk_kategori_column").hide();
			} else {
				$(".spk_kategori_column").show();
			}
		});


		//$("#PERUSAHAAN").click(function(){
				//$(".spk_kategori_column").show();

		 	//});
		//$("#PERSONAL").click(function(){
				//$(".spk_kategori_column").hide();
		 	//});

		$("#informasi").click(function() {
			if (this.checked) {
				$("#spk_stnk_nama").val($("#spk_pel_nama").val());
				$("#spk_stnk_ktp").val($("#spk_pel_identitas").val());
				$("#spk_stnk_alamat").val($("#spk_pel_alamat").val());
				$("#spk_stnk_pos").val($("#spk_pel_pos").val());
				$("#spk_stnk_telp").val($("#spk_pel_telp").val());
				$("#spk_stnk_ponsel").val($("#spk_pel_ponsel").val());
				$("#spk_stnk_email").val($("#spk_pel_email").val());
			} else {
				$("#spk_stnk_nama").val("");
				$("#spk_stnk_ktp").val("");
				$("#spk_stnk_alamat").val("");
				$("#spk_stnk_pos").val("");
				$("#spk_stnk_telp").val("");
				$("#spk_stnk_ponsel").val("");
				$("#spk_stnk_email").val("");
			}
		});

		$("#alamat").click(function() {
			if (this.checked) {
				$("#spk_stnk_alamatd").val($("#spk_pel_alamat").val());
				$("#spk_stnk_posd").val($("#spk_pel_pos").val());
			} else {
				$("#spk_stnk_alamatd").val("");
				$("#spk_stnk_posd").val("");
			}
		});

		function setType(){
			$.ajax({
				type: "GET",
				url: "{{url('api/kendaraan/type')}}"
			}).done(function(data) {
				$("#type").html("");
				jQuery.each(data, function(i, item) {
					$("#type").append("<option value='"+ item.type_id +"'>"+ item.type_nama +"</option>");
				});
				setVariant(data[0].type_id);
			});
		}

		function setPelanggan(){
			$.ajax({
				type: "GET",
				url: "{{url('api/pelanggan')}}"
			}).done(function(data) {
				$("#spk_pel").html("");
				jQuery.each(data, function(i, item) {
					$("#spk_pel").append("<option value='"+ item.pel_id +"'>"+ item.pel_nama +"</option>");
				});
			});
		}

		function setReferral(){
			$.ajax({
				type: "GET",
				url: "{{url('api/referral')}}"
			}).done(function(data) {
				$("#spkd_referral").html("");
				jQuery.each(data, function(i, item) {
					$("#spkd_referral").append("<option value='"+ item.referral_id +"'>"+ item.referral_nama +"</option>");
				});
			});
		}

		function setLeasing(){
			$.ajax({
				type: "GET",
				url: "{{url('api/leasing')}}"
			}).done(function(data) {
				$("#spk_leasing").html("");
				jQuery.each(data, function(i, item) {
					$("#spk_leasing").append("<option value='"+ item.leasing_id +"'>"+ item.leasing_nama +"</option>");
				});
			});
		}

		function setSales(){
			$.ajax({
				type: "GET",
				url: "{{url('api/karyawan/sales')}}"
			}).done(function(data) {
				$("#spk_sales").html("");
				jQuery.each(data, function(i, item) {
					$("#spk_sales").append("<option value='"+ item.sales_id +"'>"+ item.sales_karyawan +"</option>");
				});
			});
		}

		function setWarna(){
			$.ajax({
				type: "GET",
				url: "{{url('api/warna/variant')}}"
			}).done(function(data) {
				$("#spk_warna").html("");
				jQuery.each(data, function(i, item) {
					$("#spk_warna").append("<option value='"+ item.warna_id +"'>"+ item.warna_nama +"</option>");
				});
			});
		}

		function clear(){
   			$("#spk_id").val("");
   			$("#spk_tgl").val("<?php echo date('d/m/Y') ?>");
   			$("#spk_pel_identitas").val("");
   			$("#spk_pel_nama").val("");
   			$("#spk_pel_alamat").val("");
   			$("#spk_pel_pos").val("");
   			$("#spk_pel_telp").val("");
   			$("#spk_pel_ponsel").val("");
   			$("#spk_pel_email").val("");
   			$("#spk_npwp").val("");
   			$("#spk_pajak").val("");
   			$("#spk_pel_kota").val("");
   			$("#spk_stnk_nama").val("");
   			$("#spk_stnk_ktp").val("");
   			$("#spk_stnk_alamat").val("");
   			$("#spk_stnk_pos").val("");
   			$("#spk_stnk_alamatd").val("");
   			$("#spk_stnk_posd").val("");
   			$("#spk_stnk_telp").val("");
   			$("#spk_stnk_ponsel").val("");
   			$("#spk_stnk_email").val("");
   			$("#spk_fleet").val("");
   			$("#spk_pembayaran").val("");
   			$("#cashback").val("");
   			$("#keterangan").val("");
   			$('input[name="kategori"]').attr('checked', false);

   			setReferral();
   			setType();
   			setWarna();	
   			setLeasing();
   			setSales(); 
   			setVariant();
   			setPelanggan();
		}

	$("#type").change(function(){
				var id = $(this).val();
				setVariant(id);
			});
		
	$(".tambah").click(function(){
		$("#form_title").html("Spk Baru");
		$("#btn_save").html("Konfirmasi Pembelian");
		clear();
		$("#state").val("add");
	});

	$(".save").click(function(){

		var item = {
			spk_id:$("#spk_id").val(),
			spk_pel_nama:$("#spk_pel_nama").val(),
			spk_pel_identitas:$("#spk_pel_identitas").val(),
			spk_pel_lahir:$("#spk_pel_lahir").val(),
			spk_pel_alamat:$("#spk_pel_alamat").val(),
			spk_pel_pos:$("#spk_pel_pos").val(),
			spk_pel_telp:$("#spk_pel_telp").val(),
			spk_pel_ponsel:$("#spk_pel_ponsel").val(),
			spk_pel_email:$("#spk_pel_email").val(),
			spk_pel_kota:$("#spk_pel_kota").val(),
			spk_pel_kategori:$("#kategori").val(),
			spk_kendaraan:$("#spk_kendaraan").val(), 
			spk_warna:$("#spk_warna").val(),
			spk_fleet:$("#spk_fleet").val(),
			spk_stnk_nama:$("#spk_stnk_nama").val(),
			spk_stnk_ktp:$("#spk_stnk_ktp").val(),
			spk_stnk_alamat:$("#spk_stnk_alamat").val(),
			spk_stnk_pos:$("#spk_stnk_pos").val(),
			spk_stnk_alamatd:$("#spk_stnk_alamatd").val(),  
			spk_stnk_posd:$("#spk_stnk_posd").val(),
			spk_stnk_telp:$("#spk_stnk_telp").val(),
			spk_stnk_ponsel:$("#spk_stnk_ponsel").val(),
			spk_stnk_email:$("#spk_stnk_email").val(),
			spk_pembayaran:$("#spk_pembayaran").val(),
			spk_leasing:$("#spk_leasing").val(),
			spk_sales:$("#spk_sales").val(),
			spk_npwp:$("#spk_npwp").val(),
			spk_pajak:$("#pajak").val(),
			spkd_cashback:$("#cashback").val(),
			spkd_komisi:"0",
			spkd_ket:$("#keterangan").val(),
			spkd_referral:$("#spkd_referral").val(),
			_token:'{{csrf_token()}}'
		};

		var _method = "POST";
		if ($("#state").val() == "add"){
			_method = "POST";
		} else {
			_method = "PUT";
		}

		$.ajax({
			type: _method,
			url: "{{url('/api/spk/baru')}}",
			data: item
		}).fail(function(response) {
			alert("ERR-42 Pembelian gagal disimpan!, silahkan hubungi administrator");
			console.log(response);
		}).done(function(response){
			if (response==1){
	  			clear();
				loadData();
				$(".modal").modal("close");
			}else{
				alert("ERR-00 Pembelian gagal disimpan!, silahkan hubungi administrator");
			}
		}); 
	});
	clear();

});
</script>