@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Pengaturan</small>
			Pembgian No spk
		</h6>
        <ul class="header-tools right">

           <li><a href="{{url('spk/no_spk')}}" class="chip active">Pembagian No SPK</a></li>
           <li><a href="{{url('spk')}}" class="chip ">Monitoring SPK</a></li>
            <li><a href="{{url('spk/permintaan_spk')}}" class="chip ">Permintaan SPK</a></li>
            <li><a href="{{url('spk/cancel')}}" class="chip">Batal SPK</a></li>
            <li><a href="{{url('spk/permintaan_do')}}" class="chip">Permintaan DO</a></li>
             <li><a href="#tambah" class="chip tambah"><i class="fa fa-plus"></i> Tambah Nomor</a></li>
        </ul>
	</div>
	
<div class="wrapper">
    <div id="dataNospk">

    </div>  
</div>

<div id="tambah" class="modal modal-fixed-footer" style="width:400px; height: auto;">
    <form method="POST" action="{{url('/leashing')}}">
        <h6 class="modal-title blue-grey darken-1">
            <span id="form_title">Pembelian Baru</span>
            <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
        </h6>
        <div class="modal-content" style="padding:10px;position: relative;">
           @include("modules.spk.tambah_nospk")
        </div>
          <div style="padding:10px;text-align:right;background:#f5f5f5">
            <a class="waves-effect waves-light btn save"><i class="material-icons left">save</i> <span id="btn_save"> Tambah</span></a>
        </div>
     </form>
</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
    authDomain: "dealer-ms.firebaseapp.com",
    databaseURL: "https://dealer-ms.firebaseio.com",
    projectId: "dealer-ms",
    storageBucket: "dealer-ms.appspot.com",
    messagingSenderId: "950788435795"
  };
  firebase.initializeApp(config);
  firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
      console.log(error.message);
    });
</script>

<script>
    $(function() {
        function loadData() {
            var db = {
                 loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: "{{url('api/pengaturan/nospk')}}",
                        data: filter
                    });
                },

                updateItem: function(item) {
                    item['_token'] = '{{csrf_token()}}';
                    return $.ajax({
                        type: "PUT",
                        url: "{{url('/api/pengaturan/nospk')}}",
                        data: item
                    }).fail(function(response) {
                        console.log(response);
                    }).done(function(item){
                        var db = firebase.database().ref("data/tb_spk_no/" + item.spkNo_id + "/");
                        db.update({
                            spkNo_id: item.spkNo_id,
                            spkNo_min: item.spkNo_min,
                            spkNo_max: item.spkNo_max,
                            spkNo_sales: item.spkNo_sales
                        });
                    });
                },

                deleteItem: function(item) {
                    item['_token'] = '{{csrf_token()}}';
                    return $.ajax({
                        type: "DELETE",
                        url: "{{url('/api/pengaturan/nospk')}}",
                        data: item
                    }).fail(function(response) {
                        console.log(response);
                    }).done(function(response){
                        console.log(item);
                        firebase.database().ref("spk_sales/").child(item.sales_id).child(item.spk_id).remove();                   
                    });
                }
            }

            $("#dataNospk").jsGrid({
                height: "100%",
                width: "100%",
                filtering: true,

                autoload: true,
         
                deleteConfirm: "Anda yakin akan menghapus data ini?",
         
                controller: db,
         
                fields: [
                    { name: "spkNo_sales", title:"Sales", type: "text", width: 120, validate: "required" },
                    { name: "spk_id", title:"SPK", type: "number", width: 80, validate: "required" },
                    { name: "spkNo_team", title:"Team", type: "number", width: 80, validate: "required" },
                    { name: "sales_id", title:"Sales", type: "hidden", visible: false, width: 80 },
                    { type: "control", editButton:false, width:70, editing:false,  }
                ]
            });
        };

        loadData();

        $(".save").click(function(){
            var str = $("#spkNo_min").html();
            spkNo_min = Number(str.replace("17-",""));
            spkNo_max = Number($("#spkNo_max").val()) + spkNo_min;

            var item = {
                spkNo_min : spkNo_min,
                spkNo_max : spkNo_max,
                spkNo_sales : $("#spkNo_sales").val(),
                _token : '{{csrf_token()}}'
            };

            var _method = "POST";
            if ($("#state").val()==="add"){
                _method = "POST";
            }else{
                _method = "PUT";
                item['spkNo_id'] = $("#_id").val();
            }

            $.ajax({
                type: _method,
                url: "{{url('/api/pengaturan/nospk')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Pembelian gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                if (response != null){
                    var db = firebase.database().ref("spk_sales/");
                    db.update(response);
                    
                    loadData();
                    $("#tambah").modal("close");
                }else{
                    alert("ERR-00 Pembelian gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        });
    });
</script>

@endsection