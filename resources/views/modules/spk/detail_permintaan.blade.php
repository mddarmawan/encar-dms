<div id="detail_permintaan" class="modal" style="width:1180px; overflow-x: hidden;">  
    <h6 class="modal-title blue-grey darken-1">
        Detail SPK
        <span id="spk_id">SPK</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        <div class="row" style="margin:0">
                        <div class="col s12 m4">
                         <h6 style="padding:0 12px;font-weight:bold">Data Pemesan</h6>
                         <hr>
                              <table class="info">
                                    <tr>
                                          <td width="180px">Nama Pemesan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Domisili/Usaha</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_email"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode PPN</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_ppn"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">NPWP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_npwp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Faktur Pajak</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_pajak"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_fleet"></td>
                                    </tr>
                              </table>
                        </div>                        
                        <div class="col s12 m4"  style="border-left:1px solid #ddd">
                         <h6 style="padding:0 12px;font-weight:bold">Data STNK</h6>
                         <hr>
                              <table class="info">
                                    <tr>
                                          <td width="180px">Faktur STNK a/n</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">No. KTP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_identitas"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat KTP/KIMS</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Tempat Tinggal</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamatd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_posd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_email"></td>
                                    </tr>
                              </table>
                        </div>
                        <div class="col s12 m4"  style="border-left:1px solid #ddd">
                         <h6 style="padding:0 12px;font-weight:bold">Data Kendaraan</h6>
                         <hr>
                            <table class="info">
                                    <tr>
                                          <td width="180px">Variant</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">ID</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_id"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Warna</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_warna"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Keterangan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_kategori"></td>
                                    </tr>
                              </table>
                              <hr>
                              <h6 style="padding:0 12px;font-weight:bold">Keterangan</h6>
                               <hr>
                               <div>
                                   <textarea id="spk_ket_permintaan" class="no-resize" style="height: 170px; padding: 5px; font-size: 13px; color: red" readonly=""></textarea>
                              </div>
                        </div>

                  </div>
                  <div style="padding:10px;background:#f5f5f5;">
                        <center>
                              <a class="waves-effect waves-light btn orange konfirmasi" id="konfirmasi" title="Cetak Faktur">
                                    <i class="material-icons left" >done</i>
                                    <span>Konfirmasi</span>
                              </a>
                        </center>
                  </div>    
      </div>
</div>

<div id="konfirmasi_spk" class="modal"  style="width:450px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span>Konfirmasi Spk</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0;margin-bottom:10px;">
          <span style="color: red">*</span> <span style="color: black">Berikan Catatan Saat Anda Melakukan Konfirmasi</span> 
                  <div class="col m12" style="font-size:12px;margin-bottom:15px">
                         <textarea id="spk_catt_permintaan" class="" style="height: 120px" placeholder="Berikan Catatan"></textarea>
                  </div>
                  <div class="col m6">
                        <a id="tanggapi" target="_blank" class="waves-effect waves-light orange btn-large" style="width:100%"><i class="material-icons left">error_outline</i> Tanggapi</a>
                  </div>
                  <div class="col m6">
                        <a id="setujui" target="_blank" class="waves-effect waves-light green btn-large" style="width:100%"><i class="material-icons left">done</i> Setujui</a>
                  </div>
        </div>
    </div>
</div>
