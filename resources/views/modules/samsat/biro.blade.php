<div id="dataRef">

</div>

<script>
$(function() {
	var db_ref = {
        loadData: function(filter) {
            return  $.ajax({
                        type: "GET",
                        url: "{{url('api/biro')}}",
                        data: filter
                    }).done(function(data){
                        $("#biro_nama").html("");
                        for (var i in data){
                            var item = data[i];
                            $("#biro_nama").append("<option value='"+item.biro_id+"'>"+item.biro_nama+"</option>");
                        }
                    });
           
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/biro')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/biro')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/biro')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }

    };

    $("#dataRef").jsGrid({
        height: "380px",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_ref,
 
        fields: [
            { name: "biro_nama", title:"Nama Biro", type: "text", width: 150, validate: "required" },
            { name: "biro_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "biro_kodepos", title:"Kodepos", type: "text", width: 70},
            { name: "biro_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "biro_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { name: "biro_email", title:"Email", type: "text", width: 150 },
            { type: "control", width:70 }
        ]
    });
 
});
</script>