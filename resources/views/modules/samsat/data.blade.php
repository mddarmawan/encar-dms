@extends('layout')

@section('content')

<div class="content-header">
	<h6>
		<small>Penjualan</small>
		Monitoring Proses SAMSAT
	</h6>
	<ul class="header-tools right">
        <li><a href="{{url('/')}}/samsat" class="chip active">Monitoring Proses</a></li>
        <li><a href="{{url('/')}}/samsat/riwayat" class="chip">Riwayat Proses</a></li>
		<li><a href="#biro" class="chip"><i class="fa fa-user-secret"></i> Data Biro</a></li>
	</ul>
</div>

<div class="wrapper">
	<div id="data">

	</div>
</div>

<div id="biro" class="modal" style="width:960px">	
    <h6 class="modal-title blue-grey darken-1">
    	Data Biro
    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
     	@include("modules.samsat.biro")	
    </div>
</div>


<div id="detail" class="modal modal-fixed-footer" style="width:700px;height:445px">
	    <h6 class="modal-title blue-grey darken-1">
	    	Edit
	    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
	    </h6>
	    <div class="modal-content form-content" style="padding:14px">
	     	<div class="row" style="margin:0">
                <input type="hidden" id="id"/>
                <div class="col m2 s2 box">
                    <label>No SPK</label>
                    <h6 id="spk_id"></h6>
                </div>
                <div class="col m2 s2 box">
                    <label>Tanggal SPK</label>
                    <h6 id="spk_tgl"></h6>
                </div>
                <div class="col m4 s4 box">
                    <label>Nama Pemesan</label>
                    <h6 id="spk_pel_nama"></h6>
                </div>
                <div class="col m4 s4 box">
                    <label>Type</label>
                    <h6 id="variant_nama"></h6>
                </div>
                <div class="col m12 s12 box">
                    <label>STCK</label>
                    <textarea id="spkt_stck" class="no-resize"></textarea>
                </div>
                <div class="col m4 s4 box">
                    <label>Biro</label>
                    <select id="biro_nama" style="display: block;width:100%;height:23.3px;"></select>
                </div>
                <div class="col m4 s4 box">
                    <label>Tanggal Bayar Faktur</label>
                    <input type="text" id="spkt_tglfaktur" class="datepicker" readonly="" />
                </div>
                <div class="col m4 s4 box">
                    <label>Uji Type</label>
                    <input type="text" id="spkt_ujitype" class="datepicker" readonly="" />
                </div>
                <div class="col m2 s2 box">
                    <label>No Polisi</label>
                    <input type="text" class="uppercase" style="width:100%" id="spkt_nopol"/>
                </div>
                <div class="col m2 s2 box p15">
                    <input type="checkbox" class="material" id="spkt_plat" value="0" />
                    <label for="spkt_plat">PLAT</label>
                </div>
                <div class="col m2 s2 box p15">
                    <input type="checkbox" class="material" id="spkt_notis" value="0"/>
                    <label for="spkt_notis">NOTIS</label>
                </div>
                <div class="col m2 s2 box p15">
                    <input type="checkbox" class="material" id="spkt_stnk" value="0"/>
                    <label for="spkt_stnk">STNK</label>
                </div>

                <div class="col m2 s2 box" style="padding-top:9px!important">
                    <label>Waktu STNK</label>
                    <h6 id="spkt_waktu_stnk" style="text-center">0</h6>
                    <input type="hidden" id="waktu_stnk"/>
                </div>
                <div class="col m2 s2 box"  style="padding-top:9px!important">
                    <label>Status STNK</label>
                    <h6 id="spkt_status_stnk" style="text-center">0</h6>
                </div>
                <div class="col m8 s8 box">
                    <label>No BPKB</label>
                    <input type="text" class="uppercase" style="width:100%" id="spkt_nobpkb"/>
                </div>
                <div class="col m2 s2 box" style="padding-top:9px!important">
                    <label>Waktu BPKB</label>
                    <h6 id="spkt_waktu_bpkb" style="text-center">0</h6>
                    <input type="hidden" id="waktu_bpkb"/>
                </div>
                <div class="col m2 s2 box"  style="padding-top:9px!important">
                    <label>Status BPKB</label>
                    <h6 id="spkt_status_bpkb" style="text-center">0</h6>
                </div>
            </div>	
	    </div>
	    <div class="modal-footer">
	      <button type="submit" class="modal-action waves-effect waves-green btn blue save"> Simpan</button>
	    </div>
</div>

<script>
$(function() {

    function set_selected(id, selected){
        var data = document.getElementById(id).options;
        for(var i in data){
            var item = data[i];
            if (item.value == selected){
                document.getElementById(id).selectedIndex = item.index;
                break;
            }
        }
    }

    $(".save").click(function(){
        var plat=0;
        var notis=0;
        var stnk=0;
        if ($("#spkt_plat").is(":checked")){
            var plat=1;
        }
        if ($("#spkt_notis").is(":checked")){
            var notis=1;
        }
        if ($("#spkt_stnk").is(":checked")){
            var stnk=1;
        }
        var item = {
            spkt_id:$("#id").val(),
            spkt_biro:$("#biro_nama").val(),
            spkt_tglfaktur:$("#spkt_tglfaktur").val(),
            spkt_stck:$("#spkt_stck").val(),
            spkt_ujitype:$("#spkt_ujitype").val(),
            spkt_nopol:$("#spkt_nopol").val(),
            spkt_plat:plat,
            spkt_notis:notis,
            spkt_stnk:stnk,
            spkt_nobpkb:$("#spkt_nobpkb").val(),
            spkt_spk:$("#spk_id").html(),
            spkt_waktu_stnk:$("#waktu_stnk").val(),
            spkt_waktu_bpkb:$("#waktu_bpkb").val(),
            _token:'{{csrf_token()}}'
        }

        if ($("#spk_id").html() != ""){

            $.ajax({
                type: "PUT",
                url: "{{url('/api/ttbj')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Data gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                if (response==1){
                    loadData();
                    $("#detail").modal("close");
                }else{
                    alert("ERR-00 Data gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("Data belum dipilih !");
            $("#detail").modal("close");
        }
    });

    function loadData(){

	var db = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/ttbj')}}",
                    data: filter
                });
            },

    };
	
	function set_modal(item){
		$("#id").val(item.spkt_id);
		$('#spk_id').html(item.spk_id);
        $('#spk_tgl').html(item.spk_tgl);
        $('#spk_pel_nama').html(item.spk_pel_nama);
        $('#variant_nama').html(item.variant_nama);
        $('#spkt_stck').val(item.spkt_stck);
        $('#spkt_tglfaktur').val(item.spkt_tglfaktur);
        $('#spkt_ujitype').val(item.spkt_ujitype);
        set_selected("biro_nama",item.biro_id);
        $('#spkt_nopol').val(item.spkt_nopol);
        $('#spkt_nobpkb').val(item.spkt_nobpkb);
        $('#spkt_waktu_stnk').html(item.spkt_waktu_stnk);
		$('#waktu_stnk').val(item.waktu_stnk);
		$('#spkt_status_stnk').html(item.spkt_status_stnk);
		$('#spkt_waktu_bpkb').html(item.spkt_waktu_bpkb);
		$('#waktu_bpkb').val(item.waktu_bpkb);
		$('#spkt_status_bpkb').html(item.spkt_status_bpkb);
		if (item.spkt_plat==1){
			$('#spkt_plat').prop("checked",true);
		}else{
			$('#spkt_plat').prop("checked",false);
		}
		if (item.spkt_notis==1){
			$('#spkt_notis').prop("checked",true);
		}else{
			$('#spkt_notis').prop("checked",false);
		}
		if (item.spkt_stnk==1){
			$('#spkt_stnk').prop("checked",true);
		}else{
			$('#spkt_stnk').prop("checked",false);
		}

		$("#detail").modal("open");
	}

    $("#data").jsGrid({
        height: "100%",
        width: "100%",
        editing: false,
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
        rowDoubleClick:function(data){
            var item = data.item;
            set_modal(item);
        },

 
        controller: db,
 
        fields: [
            { name: "spk_id", title:"No SPK", type: "text", width: 100 },
            { name: "spk_tgl", title:"Tgl SPK", type: "text", width: 80 },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 140 },
            { name: "spk_stnk_nama", title:"BPKB a/n", type: "text", width: 140 },
            { name: "variant_nama", title:"Type", type: "text", width: 140, align:"center"},
            { name: "spkt_stck", title:"STCK", type: "text", width: 120 },
            { name: "biro_nama", title:"Biro", type: "text", width: 120, align:"center" },
            { name: "spkt_tglfaktur", title:"Tgl Faktur", type: "text", width: 90 },
            { name: "spkt_ujitype", title:"Uji Type", type: "text", width: 90},
            { name: "spkt_nopol", title:"No Polisi", type: "text", width: 100, align:"center"},
            { name: "spkt_plat", title:"plat", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_notis", title:"notis", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_stnk", title:"Stnk", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_waktu_stnk", title:"Waktu STNK", type: "text", width: 60, align:"center"},
            { name: "spkt_status_stnk", title:"Status STNK", type: "text", width: 100, align:"center"},
            { name: "spkt_nobpkb", title:"No BPKB", type: "text", width: 120},
            { name: "spkt_waktu_bpkb", title:"Waktu BPKB", type: "text", width: 60, align:"center"},
            { name: "spkt_status_bpkb", title:"Status BPKB", type: "text", width: 100, align:"center"},
            { type: "control",  width:50, align:"center", deleteButton:false, editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    
                    var $customButton = $("<a href='javascript:;' class='orange-text' title='Proses'><span class='material-icons'  style='font-size:20px'>edit</span></a>")
                        .click(function(e) {
                            set_modal(item);
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });


}
loadData();
 
});
</script>

@endsection