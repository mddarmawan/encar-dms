@extends('layout')

@section('content')

<div class="content-header">
	<h6>
		<small>Penjualan</small>
		Riwayat Proses SAMSAT
	</h6>
	<ul class="header-tools right">
        <li><a href="{{url('/')}}/samsat" class="chip">Monitoring Proses</a></li>
        <li><a href="{{url('/')}}/samsat/riwayat" class="chip active">Riwayat Proses</a></li>
        <li><a href="#biro" class="chip"><i class="fa fa-user-secret"></i> Data Biro</a></li>
	</ul>
</div>

<div class="wrapper">	
    <div id="riwayat">

    </div>
</div>

<div id="biro" class="modal" style="width:960px">	
    <h6 class="modal-title blue-grey darken-1">
    	Data Biro
    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
     	@include("modules.samsat.biro")	
    </div>
</div>


<div id="detail" class="modal modal-fixed-footer" style="width:700px;height:475px">
	    <h6 class="modal-title blue-grey darken-1">
	    	Tanda Terima BPKB
	    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
	    </h6>
	    <div class="modal-content form-content" style="padding:14px">
	     	<div class="row" style="margin:0">
                <input type="hidden" id="id"/>
                <div class="col s12 box" style="border-bottom:0;padding:0 10px!important;font-size:13px;">
                    Yang Menerima:
                </div>
                <div class="col s12 box" style="border-top:0;border-bottom:0;padding-left:25px!important">
                    <table class='info' style="margin:0;">
                        <tr>
                            <td width="90px" style="line-height: 25px">Nama</td><td width="10px" style="line-height: 25px">:</td>
                            <td><input id="nama" type="text"  class="uppercase"  placeholder="Isikan Nama Penerima"/></td>
                        </tr>
                        <tr>
                            <td width="90px" style="line-height: 25px">Alamat</td><td width="10px" style="line-height: 25px">:</td>
                            <td><input id="alamat" type="text" class="uppercase" placeholder="Isikan Alamat Penerima"/></td>
                        </tr>
                    </table>
                </div>
                <div class="col s12 box" style="border-top:0;border-bottom:0;padding:0 10px!important;font-size:13px;">
                    telah menerima BPKB dan Faktur asli dengan spesifikasi sebagai berikut:
                </div>
                <div class="col s12 box" style="border-top:0">
                    <table class='info' style="margin:0;margin-left:15px;">
                        <tr>
                            <td width="90px">BPKB a/n</td><td width="10px">:</td>
                            <td id="an" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px">No. BPKB</td><td width="10px">:</td>
                            <td id="bpkb" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px" >No. Mesin</td><td width="10px" >:</td>
                            <td id="mesin" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px" >No. Rangka</td><td width="10px" >:</td>
                            <td id="rangka" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px">No. Polisi</td><td width="10px">:</td>
                            <td id="polisi" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px">No. DH</td><td width="10px">:</td>
                            <td id="dh" class="uppercase"></td>
                        </tr>
                        <tr>
                            <td width="90px">Leasing</td><td width="10px">:</td>
                            <td id="leasing" class="uppercase"></td>
                        </tr>
                    </table>
                </div>
            </div>	
	    </div>
	    <div class="modal-footer">
	      <button type="submit" class="modal-action waves-effect waves-green btn blue save"> Simpan &amp; Cetak</button>
	    </div>
</div>

<script>
$(function() {


    $(".save").click(function(){        
        var item = {
            spkt_id:$("#id").val(),
            spkt_terima_nama:$("#nama").val(),
            spkt_terima_alamat:$("#alamat").val(),
            _token:'{{csrf_token()}}'
        }

        if ($("#id").val() != "" && $("#nama").val() != "" && $("#alamat").val() != ""){

            $.ajax({
                type: "PUT",
                url: "{{url('/api/ttbj/terima')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Data gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                if (response==1){
                    loadData();
                    var cetak = window.open('{{url("/")}}/samsat/cetak/' + $("#id").val());
                    $("#detail").modal("close");
                }else{
                    alert("ERR-00 Data gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("Nama dan Alamat belum diisi !");
            document.getElementById("nama").focus();
        }
    });


function loadData(){
	var db_log = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/ttbj/riwayat')}}",
                    data: filter
                });
            },
    };

    $("#riwayat").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering:  true,
        sorting:    true,
        autoload:   true, 
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        controller: db_log,
 
        fields: [
            { name: "spk_id", title:"No SPK", type: "text", width: 100 },
            { name: "spk_tgl", title:"Tgl SPK", type: "text", width: 80 },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 140 },
            { name: "spk_stnk_nama", title:"BPKB a/n", type: "text", width: 140 },
            { name: "variant_nama", title:"Type", type: "text", width: 140, align:"center"},
            { name: "spkt_stck", title:"STCK", type: "text", width: 120 },
            { name: "biro_nama", title:"Biro", type: "text", width: 120, align:"center" },
            { name: "spkt_tglfaktur", title:"Tgl Faktur", type: "text", width: 90 },
            { name: "spkt_ujitype", title:"Uji Type", type: "text", width: 90},
            { name: "spkt_nopol", title:"No Polisi", type: "text", width: 100, align:"center"},
            { name: "spkt_plat", title:"plat", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_notis", title:"notis", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_stnk", title:"Stnk", type: "checkbox", width: 60, align:"center"},
            { name: "spkt_waktu_stnk", title:"Waktu STNK", type: "text", width: 60, align:"center"},
            { name: "spkt_status_stnk", title:"Status STNK", type: "text", width: 100, align:"center"},
            { name: "spkt_nobpkb", title:"No BPKB", type: "text", width: 120},
            { name: "spkt_waktu_bpkb", title:"Waktu BPKB", type: "text", width: 60, align:"center"},
            { name: "spkt_status_bpkb", title:"Status BPKB", type: "text", width: 100, align:"center"},
            { name: "spkt_terima_tgl", title:"Tgl. Terima BPKB", type: "text", width: 100, align:"center"},
            { name: "spkt_terima_nama", title:"Nama Penerima", type: "text", width: 150},
            { name: "spkt_terima_alamat", title:"Alamat Penerima", type: "text", width: 200},
            { type: "control",  width:50, align:"center", deleteButton:false, editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    if (item.spkt_terima_nama != null){
                        return null;
                    }
                    
                    var $customButton = $("<a href='javascript:;' class='red-text' title='Cetak Tanda Terima BPKB'><span class='material-icons'  style='font-size:20px'>print</span></a>")
                        .click(function(e) {
                            $("#id").val(item.spkt_id);
                            $("#nama").val("");
                            $("#alamat").val("");
                            $("#an").html(item.spk_stnk_nama);
                            $("#bpkb").html(item.spkt_nobpkb);
                            $("#mesin").html(item.spkt_mesin);
                            $("#rangka").html(item.spkt_rangka);
                            $("#polisi").html(item.spkt_nopol);
                            $("#dh").html(item.spkt_dh);
                            $("#leasing").html(item.spkt_leasing);

                            $("#cetak").attr("href","{{url('/')}}/samsat/cetak/"+item.spkt_id);

                            $("#detail").modal("open");
                            document.getElementById("nama").focus();
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });
}
loadData();
 
});
</script>

@endsection