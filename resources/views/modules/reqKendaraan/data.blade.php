@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pembelian</small>
            Permintaan Pembelian Kendaraan (PO Request)
        </h6>
    </div>

<div class="wrapper">
    <div id="data">

    </div>  
</div>

<script>
$(function() {

    var db_req = {

         loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/po/request_kendaraan')}}",
                    data: filter
                });
        },
    };

    $("#data").jsGrid({
        height: "100%",
        width: "100%",
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
 
        controller: db_req,
 
        fields: [

            { name: "spk_tgl", title:"Tanggal", type: "text", width: 100, align:"center" },
            { name: "spk_id", title:"No. SPK", type: "text", width: 100, align:"center" },
            { name: "spk_pel_nama", title:"Pelanggan", type: "text", width: 100},
            { name: "karyawan_nama", title:"Sales", type: "text", width: 100 },
            { name: "team_nama", title:"Sales", type: "text", width: 100, align:"center" },
            { name: "type_nama", title:"Type", type: "text", width: 100, align:"center"},
            { name: "variant_nama", title:"Variant", type: "text", width: 150},
            { name: "warna_nama", title:"Warna", type: "text", width: 100, align:"center"},
            { name: "variant_serial", title:"ID", type: "text", width: 100, align:"center" }            
        ]
    });
 
});
</script>

@endsection