@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Request Diskon &amp; Approval
		</h6>

		<ul class="header-tools right">
			<li><a href="javascript:;" id="refresh" class="chip"><i class="fa fa-refresh"></i> Refresh</a></li>
		</ul>
	</div>
<div class="wrapper">
    <div id="dataDiskon">

    </div>
</div>
<div id="approval" class="modal">    
    <h6 class="modal-title blue-grey darken-1">
        Approval
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:10px 14px;position: relative;">
        <div class="row" style="margin:0">
            <div class="col s6">
                        <h6>
                            <span id="diskon_nama"></span><br/>
                            <small id="diskon_spk" class="orange-text lighten-1"></small>
                            <small class="right"><i class="fa fa-clock-o"></i> <span id="diskon_tgl"></span></small>
                        </h6>
                        <hr/>
                        	<table class="info payment" style="margin:0">
                                <tr>
                                    <td width="100">Sales</td><td id="sales"></td>
                                </tr>
                                <tr>
                                    <td width="100">Type</td><td id="type"></td>
                                </tr>
                            </table>
                        <hr/>
                            <table class="info payment" style="margin-bottom:0">
                                <tr>
                                    <td width="200">Cashback</td><td>Rp</td><td  class="right"><input type="text" min="0" step="1" id="cashback" class="text-right number"/></td>
                                </tr>
                                <tr>
                                    <td colspan="3">Aksesoris</td>
                                </tr>
                            </table>
                            <table id="aksesoris" class="info payment" style="margin:0">
                            </table>
                            <table class="info payment" style="margin-top:0">
                                <tr style="font-weight:bold;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
                                    <td width="200">Total</td><td>Rp</td><td class="right"><input type="text" min="0" step="1" id="total" class="text-right number"/></td>
                                </tr>
                                <tr>
                                    <td width="200">Komisi</td><td>Rp</td><td class="right"><input type="text" min="0" step="1" id="komisi" class="text-right number"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Referral</td><td id="referral" class="right" ></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <small>Keterangan</small>
                                        <p id="ket" style="margin:0"></p>
                                    </td>                                
                                </tr>
                            </table> 
            </div>
            <div class="col s6" style="padding-top:10px">
                    <input type="hidden" id="diskon_id"/>
                    <div class="input-field col s12">
                        <textarea id="catatan" class="materialize-textarea" style="height:80px"></textarea>
                        <label for="catatan">Catatan</label>
                    </div>
                    <div class="row">
                        <div class="col s6 center">
                            <button type="submit" value="1" class="btn-floating btn-large waves-effect waves-light green pulse app"><i class="material-icons">done</i></a>
                        </div>
                        <div class="col s6 center">                     
                            <button type="submit"  value="0" class="btn-floating btn-large waves-effect waves-light red app"><i class="material-icons">close</i></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    $("#refresh").click(function(){
        loadData();
    });

	$(".app").click(function(){
		var item = {
            diskon_id:$("#diskon_id").val(),
            diskon_cashback:string_format($("#cashback").val()),
            diskon_komisi:string_format($("#komisi").val()),
            diskon_status:$(this).val(),
            diskon_catatan:$("#catatan").val(),
            diskon_a0:$("#aksesoris0").val(),
            diskon_a1:$("#aksesoris1").val(),
            diskon_a2:$("#aksesoris2").val(),
            _token:'{{csrf_token()}}'
        }

        if ($("#diskon_id").val() != ""){

            $.ajax({
                type: "PUT",
                url: "{{url('/api/diskon/app')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Diskon gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                console.log(response);
                if (response==1){
                    loadData();
                    $("#approval").modal("close");
                }else{
                    alert("ERR-00 Diskon gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("Diskon belum dipilih !");
                    $("#approval").modal("close");
        }
	});

	var total=0;
	var total_aksesoris=0;
    function set_aksesoris(data){    	
        	$("#aksesoris").html('');
        	for (var j=0;j<3;j++){
        		var select='<tr><td width="20px">'+(j+1)+'</td><td width="180px"><select id="aksesoris'+j+'" data-y="'+j+'"><option value="0"></option>';   
	        	for(var i in data){
	        		var item = data[i];
	        		select +='<option value="'+ item.aksesoris_kode +'">['+ item.aksesoris_kode +'] - '+ item.aksesoris_nama +'</option>';    		
	        	}
	        	select += '</select></td><td width="20">Rp</td><td id="harga'+j+'" class="text-right">0</td></tr>';
	        	$("#aksesoris").append(select);
	        	$("#aksesoris"+j).change(function(){
	        		var y =$(this).data('y');
	        		var id = $(this).val();
	        		total_aksesoris -= string_format($("#harga"+y).html());
	        		for(var i in data){
	        			var item = data[i];
	        			if(item.aksesoris_kode == id){
	        				$("#harga"+y).html(number_format(item.aksesoris_harga));
	        				total_aksesoris += item.aksesoris_harga;
	        				break;
	        			}
	        		}

	        		$("#total").val(number_format(total_aksesoris + string_format($('#cashback').val())));

	        		if (id==0){
	        			$("#harga"+y).html("0");
	        		}
	        	});
	        }
    }

    function set_selected(id, selected){
    	var data = document.getElementById(id).options;
    	for(var i in data){
    		var item = data[i];
    		if (item.value == selected){
    			document.getElementById(id).selectedIndex = item.index;
  				return true;
    		}
    	}
    	return false;
    }


    $('#cashback').change(function(){
    	$('#total').val(number_format(string_format($(this).val()) + total_aksesoris));
    });
    $('#total').change(function(){
    	$('#cashback').val(number_format(string_format($(this).val()) - total_aksesoris));
    });

    function loadData(){
		var db_diskon = {
	        loadData: function(filter) {
				return $.ajax({
	                type: "GET",
	                url: "{{url('api/diskon/request')}}",
	                data: filter
	            });
	        },
	    };

	    $("#dataDiskon").jsGrid({
	        height: "98%",
	        width: "100%",
	 
	        sorting: true,
	        autoload: true,
	        paging: true,
	        noDataContent: "Tidak Ada Data",
	 
	        deleteConfirm: "Anda yakin akan menghapus data ini?",
	 
	        controller: db_diskon,
	 
	        fields: [
	            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
	            { name: "spk_id", title:"No SPK", type: "text", width: 100, align:"center" },
	            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 100},
	            { name: "spk_sales", title:"Sales", type: "text", width: 100},
	            { name: "spk_cashback", title:"Cashback", type: "number", width: 100, align:"right" },
	            { name: "spk_taksesoris", title:"Aksesoris", type: "number", width: 100, align:"right" },
	            { name: "spk_komisi", title:"Komisi", type: "number", width: 100, align:"right"},
	            { name: "spk_total", title:"Total", type: "number", width: 100, align:"right" },
	            { name: "spk_ket", title:"Keterangan", type: "text", width: 140},
	            { name: "spk_ref", title:"Nama Referral", type: "text", width: 120},
	            { type: "control", deleteButton:false, editButton:false, width:50, align:"center", itemTemplate: function(value, item) {
	                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
	                        
	                        var $customButton = $("<a href='javascript:;' class='green-text matching' title='Matching'><span class='material-icons'  style='font-size:20px'>done</span></a>")
	                            .click(function(e) {
	                            	$.ajax({
							            type: "GET",
							            url: "{{url('api/diskon/aksesoris/')}}/" + item.spk_type_id
							        }).done(function(data){

		                            	$("#diskon_id").val(item.spk_diskon);
		                            	$("#diskon_tgl").html(item.spk_tgl);
								        $("#diskon_spk").html(item.spk_id);
								        $("#diskon_nama").html(item.spk_pel_nama);
								        $("#sales").html(item.spk_sales);
								        $("#type").html(item.spk_type);
								        $("#cashback").val(item.spk_cashback);
	    								set_aksesoris(data);
								        //$("#aksesoris").html(item.spk_taksesoris);
								        total_aksesoris = 0;
								        var aksesoris = item.spk_aksesoris;
								        if (aksesoris.length>0){
								        	for (var i in aksesoris){
								        		var item_a = aksesoris[i];
								        		if(set_selected("aksesoris"+i, item_a.spka_kode)){
									        		$("#harga"+i).html(number_format(item_a.spka_harga));

									        		total_aksesoris+= item_a.spka_harga;
									        	}else{
									        		$("#harga"+i).html('0');
									        	}
								        	}
								        }
		            					total= string_format(item.spk_cashback) + total_aksesoris;

								        $("#komisi").val(item.spk_komisi);
								        $("#total").val(number_format(total));
								        $("#referral").html(item.spk_ref);
								        $("#ket").html(item.spk_ket);

		                                $("#approval").modal("open");
		                                e.stopPropagation();
	                            	});
	                            });
	                        

	                        return $result.add($customButton); }
	                    }
	        ]
	    });
	}
	loadData();
 
});
</script>

@endsection