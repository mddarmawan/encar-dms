@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Diskon
		</h6>

		<ul class="header-tools right">
			<li><a href="javascript:;" id="refresh" class="chip"><i class="fa fa-refresh"></i> Refresh</a></li>
		</ul>
	</div>
<div class="wrapper">
    <div id="dataDiskon"">

    </div>
</div>
<div id="detail" class="modal" style="width:500px">    
    <h6 class="modal-title blue-grey darken-1">
        Diskon
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:10px 14px;position: relative;">
                        <h6>
                            <span id="diskon_nama"></span><br/>
                            <small id="diskon_spk" class="orange-text lighten-1"></small>
                            <small class="right"><i class="fa fa-clock-o"></i> <span id="diskon_tgl"></span></small>
                        </h6>
                        <hr/>
                        	<table class="info payment" style="margin:0">
                                <tr>
                                    <td width="100">Sales</td><td id="sales"></td>
                                </tr>
                                <tr>
                                    <td width="100">Type</td><td id="type"></td>
                                </tr>
                            </table>
                        <hr/>
                            <table class="info payment" style="margin-bottom:0">
                                <tr>
                                    <td width="300">Cashback</td><td>Rp</td><td id="cashback"  class="text-right"></td>
                                </tr>
                                <tr>
                                    <td colspan="3">Aksesoris</td>
                                </tr>
                            </table>
                            <table id="aksesoris" class="info payment" style="margin:0">
                            </table>
                            <table class="info payment" style="margin-top:0">
                                <tr>
                                    <td width="300">Komisi</td><td>Rp</td><td id="komisi" class="text-right"></td>
                                </tr>
                                <tr style="font-weight:bold;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
                                    <td width="300">Total</td><td>Rp</td><td id="total" class="right"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Referral</td><td id="referral" class="right" ></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <span>Keterangan</span>
                                        <p id="ket" style="margin:0"></p>
                                    </td>                                
                                </tr>
                            </table> 
    </div>
</div>

<script>
$(function() {
    $("#refresh").click(function(){
        loadData();
    });

	var total=0;

    function loadData(){
		var db_diskon = {
	        loadData: function(filter) {
				return $.ajax({
	                type: "GET",
	                url: "{{url('api/diskon')}}",
	                data: filter
	            });
	        },
	    };

	    $("#dataDiskon").jsGrid({
	        height: "98%",
	        width: "100%",
	 
	        sorting: true,
	        autoload: true,
	        paging: true,
	        noDataContent: "Tidak Ada Data",
	 
	        deleteConfirm: "Anda yakin akan menghapus data ini?",
	 
	        controller: db_diskon,
	 
	        fields: [
	            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
	            { name: "spk_id", title:"No SPK", type: "text", width: 100, align:"center" },
	            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 100},
	            { name: "spk_sales", title:"Sales", type: "text", width: 100},
	            { name: "spk_cashback", title:"Cashback", type: "number", width: 100, align:"right" },
	            { name: "spk_taksesoris", title:"Aksesoris", type: "number", width: 100, align:"right" },
	            { name: "spk_komisi", title:"Komisi", type: "number", width: 100, align:"right"},
	            { name: "spk_total", title:"Total", type: "number", width: 100, align:"right" },
	            { name: "spk_ket", title:"Keterangan", type: "text", width: 140},
	            { name: "spk_ref", title:"Nama Referral", type: "text", width: 120},
	            { type: "control", deleteButton:false, editButton:false, width:50, align:"center", itemTemplate: function(value, item) {
	                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
	                        
	                        var $customButton = $("<a href='#approval' class='blue-text matching' title='Matching'><span class='material-icons'  style='font-size:20px'>search</span></a>")
	                            .click(function(e) {
	            					total= string_format(item.spk_cashback);
	                            	$("#diskon_tgl").html(item.spk_tgl);
							        $("#diskon_spk").html(item.spk_id);
							        $("#diskon_nama").html(item.spk_pel_nama);
							        $("#sales").html(item.spk_sales);
							        $("#type").html(item.spk_type);
							        $("#cashback").html(item.spk_cashback);
							        $("#aksesoris").html('');
							        var aksesoris = item.spk_aksesoris;
							        if (aksesoris.length>0){
							        	for (var i in aksesoris){
							        		var item_a = aksesoris[i];
							        		$("#aksesoris").append('<tr><td width="20px">'+(parseInt(i)+1)+'</td><td width="280px">['+ item_a.spka_kode +'] - '+ item_a.spka_nama +'</td><td width="20">Rp</td><td class="text-right">'+number_format(item_a.spka_harga)+'</td></tr>');

							        		total+= item_a.spka_harga;
							        	}
							        }
							        $("#komisi").html(item.spk_komisi);
							        $("#total").html(number_format(total));
							        $("#referral").html(item.spk_ref);
							        $("#ket").html(item.spk_ket);

	                                $("#detail").modal("open");
	                                e.stopPropagation();
	                            });
	                        

	                        return $result.add($customButton); }
	                    }
	        ]
	    });
	}
	loadData();
 
});
</script>

@endsection