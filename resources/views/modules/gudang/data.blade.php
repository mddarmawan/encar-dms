@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Persediaan</small>
            Gudang Kendaraan
        </h6>
    </div>

<div class="wrapper">
    <div id="dataEkspedisi">

    </div>  
</div>

<script>
$(function() {

    var db = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/gudang')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/gudang')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/gudang')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/gudang')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };

    $("#dataEkspedisi").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "gudang_nama", title:"Nama ekspedisi", type: "text", width: 150, validate: "required" },
            { name: "gudang_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "gudang_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "gudang_telp", title:"Telepon", type: "number", width: 100 },
            { name: "gudang_email", title:"Email", type: "text", width: 100 },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

@endsection