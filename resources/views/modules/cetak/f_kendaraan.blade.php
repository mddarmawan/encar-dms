<style>
	body{
		margin:0;
		padding:0;
		font-size:14px;
		/*font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;*/
	}
	td, th{
		font-size:12px;
	}
	h5{
		margin: 5px 0 20px;
		font-weight:400;
	}
	.text-center{
		text-align:center;
	}
	.text-right{
		text-align:right;
	}
	table{
		width:100%;		
	}
	table, td, th, tr{
		border:0;
		border-spacing: 0;
		border-color:#fff;
		padding:0;
		border-collapse: collapse;
		vertical-align:top;
	}
	table th{
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:4px solid #ddd;
		padding:3px 7px;
		vertical-align:middle;
		background:#f5f5f5;
		border-spacing: 0;
		border-collapse: collapse;
	}
	
	table tfoot th{
		border-bottom:1px solid #ddd;
		border-top:4px solid #ddd;
	}
	
	.table td{
		padding:3px 7px;
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:1px solid #ddd;		
	}

	.bold{
		font-weight:bold;
	}
</style>
<page>
	<body>
		<table style="width: 100%">
			<tr>
				<td style="width: 450px;">
					<p style="margin-top: 20px;">
						PT. Encartha Indonesia <br>
						JL.Angkatan 45 Palembang 30137 <br>
						Telp. 0711-374000 fax. 0711-374500 <br>
						NPWP : 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008 <br>
						BKB : 01.596.627.6.308.000 <br><br>
						No. BPK : 17-00834 <br>
						No. Indent : TERIOS <br>
						Referensi : MUHAMMAD MUSHAWWIR
					</p>
				</td>
				<td>
					<h4 style="margin-left: 20px;">FAKTUR KENDARAAN</h4>
					Nomor : BP17-36660 <br>
					Tanggal : 16 Januari 2017 <br><br>
					Pemesan : <br>
					VIRGIANA ASTUTI<br>
					JL. AHMAD YANI RT/RW 17/08<br>
					KEL.AIR BARU KEC.TALANG KECAMATAN<br>
					BANYUASIN
				</td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin-bottom: 10px;"></div>
		<table style="width: 100%">
			<tr>
				<td style="width: 450px;">
					<p style="margin:0; margin-top: 2px;">Keterangan Kendaraan : 1 (satu) Unit Daihatsu TERIOS R MT </p>
					<p style="margin:0; margin-top: 2px;">Nomor Rangka : ANWUD00WH27ASJIDA92LL </p>
					<p style="margin:0; margin-top: 2px;">Nomor Mesin : 4782ZXF82898 </p>
					<p style="margin:0; margin-top: 2px;">Tipe : AS8U9EWDSJJ </p>
					<p style="margin:0; margin-top: 2px;">Warna : BLACK METALIC </p>
					<p style="margin:0; margin-top: 2px;">No. DH : 20170591 </p>
				</td>
				<td>
					<p style="margin:0; margin-top: 2px;">&nbsp;</p>
					<p style="margin:0; margin-top: 2px;">STNK atas Nama : </p>
					<p style="margin:0; margin-top: 2px;">VIRGINIA ASTUTI</p>
					<p style="margin:0; margin-top: 2px;">JL. AHMAD YANI RT/RW 17/08 KEL.AIR BATU KEC. TALANG KE</p>
					<p style="margin:0; margin-top: 2px;">BANYUASIN</p>
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td style="width: 500px;"><b>KETERANGAN</b></td>
				<td><p style=""><b>HARGA</b></p></td>
			</tr>
			<tr>
				<td>Harga Off The Road</td>
				<td><p style="text-align: right;"> 213.550.000 </p></td>
			</tr>
			<tr>
				<td>BBN</td>
				<td><p style="text-align: right;"> 20.100.000 </p></td>
			</tr>
			<tr>
				<td>On The Road</td>
				<td><p style="text-align: right;"> 233.650.000 </p></td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
		<table width="100%">
			<tr>
				<td style="width: 500px;">26-05-2017</td>
				<td><p style="text-align: right;"> 3.500.000 </p></td>
			</tr>
			<tr>
				<td>12-06-2017</td>
				<td><p style="text-align: right;"> 22.816.000 </p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">___________________</p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">&nbsp;</p></td>
			</tr>
			<tr>
				<td>Pembayaran Konsumen</td>
				<td><p style="text-align: right;"> 26.316.000 </p></td>
			</tr>
			<tr>
				<td>Cash Back Dealer</td>
				<td><p style="text-align: right;"> 26.000.000 </p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">___________________</p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">&nbsp;</p></td>
			</tr>
			<tr>
				<td>Bayar I</td>
				<td><p style="text-align: right;"> 52.316.000 </p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">___________________</p></td>
			</tr>
			<tr>
				<td></td>
				<td><p style="text-align: right;">&nbsp;</p></td>
			</tr>
			<tr>
				<td>Dropping</td>
				<td><p style="text-align: right;"> 181.333.500</p></td>
			</tr>
		</table>
		<table width="40%" style="margin-top: 150px;">
			<tr>
				<td width="10%">Jenis Pembayaran</td>
				<td width="2%"> : </td>
				<td width="30%">CREDIT</td>
			</tr>
			<tr>
				<td>Leasing</td>
				<td> : </td>
				<td>ACC / KOMBINASI</td>
			</tr>
			<tr>
				<td>Jangka Waktu</td>
				<td> : </td>
				<td>5 THN</td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
		<table style="width: 100%;">
			<tr>
				<td>
					<p style="float: left; margin-left: 5%;">Dibuat oleh
						<br><br><br><br>
						__________________
						</p>
						<p style="float: right; margin-right: 5%;">Pimpinan
						<br><br><br><br>
						__________________
					</p>
				</td>
			</tr>
		</table>
	</body>
</page>