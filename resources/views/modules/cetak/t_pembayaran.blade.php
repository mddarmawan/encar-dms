<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			body{
				font-family: arial;
			}
			.head{
				width: 100%;
				border-top:1px solid #000;
				border-bottom:1px solid #000;
				font-size: 15px;
			}
			.bulat{
				background-color: #000;
				color: #fff;
				border-radius: 30px;
				width: 15px;
				height: 15px;
				display: inline-block;
				-webkit-print-color-adjust: exact; 
			}
			.bulat>center{
				margin-top: 2px;
			}
			.col-3{
				width: 55%;
				display: inline-block;
			}
			table{border-collapse:collapse}
			.tr{
				border-top:1px solid #000;
				border-bottom:1px solid #000;
			}

		</style>
	</head>
	<body>
		<div style="">
			<div style="display: inline-block; float: right;">
				<tt>
				<h2 style="margin: 0;"><tt>FAKTUR KENDARAAN</tt></h2>
				<table class="table" style="font-size: 13px;">
					<tr>
						<td class="td"><a style="margin: 30px 0;">Nomor</a></td>
						<th> : </th>
						<td class="td_isi"> BP17-36660</td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">Tanggal</a></td>
						<th> : </th>
						<td> 16 Januari 2017</td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>
						<th></th>
						<td></td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>
						<th></th>
						<td></td>
					</tr>
				</table>
				<p style="margin: 0;">Pemesan : <br>
				VIRGIANA ASTUTI<br>
				JL. AHMAD YANI RT/RW 17/08<br>
				KEL.AIR BARU KEC.TALANG KE<br>
				BANYUASIN
				</p>
				</tt>
			</div>
			<div style="display: inline-block; float: left; margin-top: 20px;">
				<tt>
				<p style="margin: 0; margin-bottom: 3px;">PT. Encartha Indonesia</p>
				<p style="margin: 0; margin-bottom: 2px;">JL.Angkatan 45 Palembang 30137</p>
				<p style="margin: 0; margin-bottom: 2px;">Telp. 0711-374000 fax. 0711-374500</p>
				<table class="table" style="font-size: 13px;">
					<tr>
						<td class="td"><a>NPWP</a></td>
						<th> : </th>
						<td class="td_isi"> 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008</td>
					</tr>
					<tr>
						<td class="td"><a>BKB</a></td>
						<th> : </th>
						<td> 01.596.627.6.308.000</td>
					</tr>
				</table>
				<table class="table" style="font-size: 13px; margin: 10px 0;">
					<tr>
						<td class="td"><a style="margin: 30px 0;">No. BPK</a></td>
						<th> : </th>
						<td class="td_isi"> 17-00834</td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">No. Indent</a></td>
						<th> : </th>
						<td> TERIOS</td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">Referensi</a></td>
						<th> : </th>
						<td> MUHAMMAD MUSHAWWIR</td>
					</tr>
				</table>
				</tt>
			</div>
		</div>	
			<tt>

				<table width="99%">
				</table>
				<div style="border-top: 1px solid #000; margin-bottom: 10px;"></div>
				<div style="margin-bottom: 140px;">
					<div style="width: 70%; float: left;">
						<table width="100%">
							<tr>
								<td width="10%">Keterangan Kendaraan</td>
								<td width="2%"> : </td>
								<td width="30%">1 (satu) Unit Daihatsu TERIOS R MT</td>
							</tr>
							<tr>
								<td width="15%">Nomor Rangka</td>
								<td width="2%"> : </td>
								<td width="30%">ANWUD00WH27ASJIDA92LL</td>
							</tr>
							<tr>
								<td width="10%">Nomor Mesin</td>
								<td width="2%"> : </td>
								<td width="30%">4782ZXF82898</td>
							</tr>
							<tr>
								<td width="15%">Tipe</td>
								<td width="2%"> : </td>
								<td width="30%">AS8U9EWDSJJ</td>
							</tr>
							<tr>
								<td width="15%">Warna</td>
								<td width="2%"> : </td>
								<td width="30%">BLACK METALIC</td>
							</tr>
							<tr>
								<td width="15%">No. DH</td>
								<td width="2%"> : </td>
								<td width="30%">20170591</td>
							</tr>
						</table>
					</div>
					<div style="width: 30%; float: right;">
						<p style="margin:0; margin-top: 2px;">&nbsp;</p>
						<p style="margin:0; margin-top: 2px;">STNK atas Nama : </p>
						<p style="margin:0; margin-top: 2px;">VIRGINIA ASTUTI</p>
						<p style="margin:0; margin-top: 2px;">JL. AHMAD YANI RT/RW 17/08 KEL.AIR BATU KEC. TALANG KE</p>
						<p style="margin:0; margin-top: 2px;">BANYUASIN</p>
					</div>
				</div>
				<table width="100%">
					<tr>
						<td width="50%"><b>KETERANGAN</b></td>
						<th width="50%"><a style="float: right; margin-right: 30px;"> HARGA </a></th>
					</tr>
					<tr>
						<td width="50%">Harga Off The Road</td>
						<td width="50%"><a style="float: right;"> 213.550.000 </a></td>
					</tr>
					<tr>
						<td width="50%">BBN</td>
						<td width="50%"><a style="float: right;"> 20.100.000 </a></td>
					</tr>
					<tr>
						<td width="50%">On The Road</td>
						<td width="50%"><a style="float: right;"> 233.650.000 </td>
					</tr>
				</table>
				<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
				<table width="100%">
					<tr>
						<td width="50%">26-05-2017</td>
						<td width="50%"><a style="float: right;"> 3.500.000 </a></td>
					</tr>
					<tr>
						<td width="50%">12-06-2017</td>
						<td width="50%"><a style="float: right;"> 22.816.000 </a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">___________________</a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">&nbsp;</a></td>
					</tr>
					<tr>
						<td width="50%">Pembayaran Konsumen</td>
						<td width="50%"><a style="float: right;"> 26.316.000 </a></td>
					</tr>
					<tr>
						<td width="50%">Cash Back Dealer</td>
						<td width="50%"><a style="float: right;"> 26.000.000 </a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">___________________</a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">&nbsp;</a></td>
					</tr>
					<tr>
						<td width="50%">Bayar I</td>
						<td width="50%"><a style="float: right;"> 52.316.000 </a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">___________________</a></td>
					</tr>
					<tr>
						<td width="50%"></td>
						<td width="50%"><a style="float: right;">&nbsp;</a></td>
					</tr>
					<tr>
						<td width="50%">Dropping</td>
						<td width="50%"><a style="float: right;"> 181.333.500</a></td>
					</tr>
				</table>
				<table width="40%" style="margin-top: 150px;">
					<tr>
						<td width="10%">Jenis Pembayaran</td>
						<td width="2%"> : </td>
						<td width="30%">CREDIT</td>
					</tr>
					<tr>
						<td>Leasing</td>
						<td> : </td>
						<td>ACC / KOMBINASI</td>
					</tr>
					<tr>
						<td>Jangka Waktu</td>
						<td> : </td>
						<td>5 THN</td>
					</tr>
				</table>
				<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
				<div style="width: 100%;">
					<center>
						<p style="float: left; margin-left: 5%;">Dibuat oleh
						<br><br><br><br>
						__________________
						</p>
						<p style="float: right; margin-right: 5%;">Pimpinan
						<br><br><br><br>
						__________________
						</p>

					</center>
				</div>
			</tt>
	</body>
</html>