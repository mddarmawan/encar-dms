@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            Pelanggan
        </h6>
    </div>

<div class="wrapper">
    <div id="data">

    </div>  
</div>

<div id="detail" class="modal"  style="width:1000px;">  
    <h6 class="modal-title blue-grey darken-1">
        Data Pelanggan
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0; position: relative;">
        @include("modules.pelanggan.detail")  
    </div>
</div>

<script>
    function detail(e){
        var id = $(e).attr("data-id");

        $.ajax({
            type: "GET",
            url: "{{url('api/pelanggan/')}}/"+id
        }).done(function(json) {
            var pemesan = json.pemesan;
            console.log(json);
            $("#pel_nama").html(pemesan.pel_nama);
            $("#pel_lahir").html(pemesan.pel_lahir);
            $("#pel_kota").html(pemesan.pel_kota);
            $("#pel_alamat").html(pemesan.pel_alamat);
            $("#pel_pos").html(pemesan.pel_pos);
            $("#pel_telp").html(pemesan.pel_telp);
            $("#pel_ponsel").html(pemesan.pel_ponsel);
            $("#pel_email").html(pemesan.pel_email);
           
            var db_spk = {

                loadData: function(filter) {
                        return json.spk;
                },

            };

            db_spk.pembayaran = [
            {
                "pembayaran_id": "",
                "pembayaran_nama": "",           
            },

            {
                "pembayaran_id": 1,
                "pembayaran_nama": "CREDIT",           
            },

            {
                "pembayaran_id": 2,
                "pembayaran_nama": "CASH",           
            },
        ];

            $("#dataSPK").jsGrid({
                height: "150px",
                width: "100%",
         
                filtering: false,
                sorting: true,
                autoload: true,
         
                controller: db_spk,
         
                fields: [
                    { name: "spk_tgl", title:"Tanggal", type: "text", width:100, align:"center" },
                    { name: "spk_id", title:"NO SPK", type: "text", width: 80, align:"center" },
                    { name: "spk_stnk_nama", title:"Nama STNK", type: "text", width: 140 , align:"center"},
                    { name: "karyawan_nama", title:"Sales", type: "text", width: 140, align:"center" },
                    { name: "variant_nama", title:"Type", type: "text", width: 180, align:"center" },
                    { name: "spk_pembayaran", title:"Pembayaran", type: "select", items: db_spk.pembayaran, valueField: "pembayaran_id", textField: "pembayaran_nama", width: 100, align:"center" },
                    { name: "leasing_nama", title:"Leasing", type: "text", width: 100, align:"center" },
                    { name: "spk_do", title:"Tgl. Do", type: "text", width: 100, align:"center" }
                ]
            });

        });
    };

    $(function() {

        var db_pelanggan = {

             loadData: function(filter) {
                return $.ajax({
                            type: "GET",
                            url: "{{url('api/pelanggan')}}",
                            data: filter
                        });
            },
        };

        $("#data").jsGrid({
            height: "100%",
            width: "100%",
     
            sorting: true,
            filtering: true,
            autoload: true,
            paging: true,
            pageSize: 30,
            pageButtonCount: 5,
            noDataContent: "Tidak Ada Data",
     
     
     
            controller: db_pelanggan,
     
            fields: [
                { name: "pel_nama", title:"Nama", type: "text", width: 120, align:"" },
                { name: "pel_alamat", title:"Alamat", type: "text", width: 260, align:"" },
                { name: "pel_lahir", title:"Tgl. Lahir", type: "text", width: 100},
                { name: "pel_kota", title:"Kota", type: "number", width: 100, align:"center" },
                { name: "pel_pos", title:"Kode Pos", type: "text", width: 100, align: "center" },
                { name: "pel_telp", title:"No. Telp", type: "text", width: 100, align:"center"},
                { name: "pel_ponsel", title:"Ponsel", type: "number", width: 100, align:"center" },
                { name: "pel_email", title:"Email", type: "text", width: 150, align:"center"},
                { name: "pel_sales", title:"Sales", type: "number", width: 170, align:"center" },
               { name: "detail", title:"", width:50, align:"center" }
            ]
        });
     
    });
</script>

@endsection