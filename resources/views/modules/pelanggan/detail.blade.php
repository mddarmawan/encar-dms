         <div class="row" style="margin:0;padding:0;border-top:1px solid #ddd">
          
            <div class="col s12 m6">
                <table class="info ">
                    <tr>
                        <td width="180px">Nama Pelanggan</td>
                        <td width="10px">:</td>
                        <td class="bold" id="pel_nama"></td>
                    </tr>
                     <tr>
                        <td width="120px">Tanggal Lahir</td>
                        <td width="10px">:</td>
                        <td class="bold" id="pel_lahir"></td>
                    </tr>
                    <tr>
                        <td width="180px">Alamat Domisili/Usaha</td>
                        <td width="10px">:</td>
                        <td class="bold" id="pel_alamat"></td>
                    </tr>
                    
                </table>
            </div>
            <div class="col m6" style="padding:0;border-left:1px solid #ddd">
                <div class="row" style="margin:0;border-top:1px solid #ddd">
                    <div class="col m12">
                        <table class="info">
                             <tr>
                                <td width="180px">Kode Pos</td>
                                <td width="10px">:</td>
                                <td class="bold" id="pel_pos"></td>
                            </tr>
                            <tr>
                                <td width="180px">Ponsel</td>
                                <td width="10px">:</td>
                                <td class="bold" id="pel_ponsel"></td>
                            </tr>
                            <tr>
                                <td width="180px">Email</td>
                                <td width="10px">:</td>
                                <td class="bold" id="pel_email"></td>
                            </tr>
                            <tr>
                                <td width="120px">Kota</td>
                                <td width="10px">:</td>
                                <td class="bold" id="pel_kota"></td>
                            </tr>
                        </table> 
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin:0;padding:0;border-top:1px solid #ddd; height: auto;"> 
        </div>
        <h6 style="padding:0 12px;font-weight:bold">Daftar SPK</h6>
         <div class="row" style="margin:0;padding:0;border-top:1px solid #ddd; height: auto;overflow-y:hidden"> 
            @include("modules.pelanggan.data_spk")
        </div>
    