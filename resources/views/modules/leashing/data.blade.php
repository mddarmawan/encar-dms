@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            Tagihan Leasing
        </h6>
        <ul class="header-tools right">
            <li><a href="#leashing" class="chip"><i class="fa fa-car"></i> Master Leasing</a></li>
            <li><a href="#asuransi" class="chip"><i class="fa fa-info"></i> Master Asuransi</a></li>
            <li><a href="javascript:;" class="chip" onclick="loadData()"><i class="fa fa-refresh"></i> Refresh</a></li>
        </ul>
    </div>

<div class="wrapper">
    <div id="dataLeasing">

    </div>
</div>

<div id="leashing" class="modal" style="width:800px;">  
    <h6 class="modal-title blue-grey darken-1">
        Data Leasing
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        @include("modules.leashing.data_leashing")  
    </div>
</div>

<div id="asuransi" class="modal"  style="width:800px;">  
    <h6 class="modal-title blue-grey darken-1">
        Data Asuransi
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        @include("modules.leashing.data_asuransi")   
    </div>
</div>


<div id="setasuransi" class="modal"  style="width:350px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span class="left">Asuransi<br/><small class="spk"></small></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
            <select type="text" class="asuransi fulldate">

            </select>
    </div>
    <div class="modal-footer" style="padding:0 15px">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat left">Batal</a>
        <a href="javascript:;" onclick="asuransi_save(this)" class="modal-action modal-close  waves-effect waves-green btn blue" style="margin-bottom: 0">Simpan</a>
    </div>
</div>


<div id="cetak" class="modal"  style="width:350px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span class="left">Cetak Tagihan<br/><small class="spk"></small></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0">
            <a id="ctagihan" class="waves-effect waves-light btn-large col m12" style="margin-bottom:15px"><i class="material-icons left">print</i> Surat Tagihan</a>
            <a id="cdp" class="waves-effect waves-light btn-large col m12" style="margin-bottom:15px"><i class="material-icons left">print</i> Kwitansi DP</a>
            <a id="clunas" class="waves-effect waves-light btn-large col m12"><i class="material-icons left">print</i> Kwitansi Pelunasan</a>
        </div>
    </div>
</div>

<div id="tagihan" class="modal"  style="width:350px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span class="left">Tanggal Tagihan<br/><small class="spk"></small></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="input-field" style="margin:0">
            <i class="fa-prefix fa fa-calendar blue"></i>
            <input type="text"  class="tgl datepicker fulldate" readonly="" placeholder="DD/MM/YYYY" value="{{date('d/m/Y')}}" />
        </div>
    </div>
    <div class="modal-footer" style="padding:0 15px">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat left">Batal</a>
        <a href="javascript:;" onclick="tagihan_save(this)" class="modal-action modal-close  waves-effect waves-green btn blue" style="margin-bottom: 0">Simpan</a>
    </div>
</div>


<div id="lunas" class="modal"  style="width:350px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span class="left">Tanggal Pelunasan<br/><small class="spk"></small></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="input-field" style="margin:0">
            <i class="fa-prefix fa fa-calendar blue"></i>
            <input type="text"  class="tgl datepicker fulldate" readonly="" placeholder="DD/MM/YYYY" value="{{date('d/m/Y')}}" />
        </div>
    </div>
    <div class="modal-footer" style="padding:0 15px">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat left">Batal</a>
        <a href="javascript:;" onclick="lunas_save(this)" class="modal-action modal-close  waves-effect waves-green btn blue" style="margin-bottom: 0">Simpan</a>
    </div>
</div>


<div id="refund" class="modal"  style="width:350px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span class="left">Refund<br/><small class="spk"></small></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="input-field" style="margin:0">
            <i class="fa-prefix fa fa-calendar blue"></i>
            <input type="text"  class="tgl datepicker fulldate" readonly="" placeholder="DD/MM/YYYY" value="{{date('d/m/Y')}}" />
        </div>
        <label>Jumlah Refund: Rp</label>
        <input type="text" min="0" step="1000"  class="number jumlah fulldate" placeholder="0" />
    </div>
    <div class="modal-footer" style="padding:0 15px">
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat left">Batal</a>
        <a href="javascript:;" onclick="refund_save(this)" class="modal-action modal-close  waves-effect waves-green btn blue" style="margin-bottom: 0">Simpan</a>
    </div>
</div>

<script>
function loadData() {

    var db_leashing = {

        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/spk/leasing')}}",
                    data: filter
                });
        },
    };

    db_leashing.status = [
            {
            "status_id": "",
            "status_nama": "",           
        },

        {
            "status_id": "0",
            "status_nama": "FAKTUR",           
        },

        {
            "status_id": "1",
            "status_nama": "DO",           
        },

        {
            "status_id": "2",
            "status_nama": "CETAK",           
        },

        {
            "status_id": "3",
            "status_nama": "TAGIHAN",           
        },

        {
            "status_id": "4",
            "status_nama": "LUNAS",           
        },

        {
            "status_id": "5",
            "status_nama": "REFUND",           
        },
    ];

    
    $("#dataLeasing").jsGrid({
        height: "100%",
        width: "100%",
        editing: false,
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_leashing,
 
        fields: [
            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
            { name: "karyawan_nama", title:"Sales", type: "text", width: 100 },
            { name: "team_nama", title:"Team", type: "text", width: 100, align:"center" },
            { name: "leasing_nama", title:"Leashing", type: "text", width: 100 },
            { name: "spkl_asuransi", title:"Asuransi", type: "text", width: 120, align:"center" },
            { name: "spkl_jenis_asuransi", title:"Jenis Asuransi", type: "text", width: 100, align:"center" },
            { name: "spkl_angsuran", title:"Angsuran", type: "text", width: 80, align:"right" },
            { name: "spkl_waktu", title:"Jangkah Waktu", type: "number", width: 100, align:"center" },
            { name: "spkl_dp", title:"DP", type: "text", width: 100, align:"center"},
            { name: "spkl_droping", title:"Dropping", type: "number", width: 100, align:"right"},
            { name: "spkl_piutang", title:"Piutang", type: "text", width: 120, align:"right" },
            { name: "spkl_status", title:"Status", type: "select", items: db_leashing.status, valueField: "status_id", textField: "status_nama", width: 110, align:"center" },
            { name: "spkl_faktur", title:"Tgl. Faktur", type: "text", width: 120, align:"center" },
            { name: "spkl_wgi", title:"", type: "text", width: 70, align:"center" },
            { name: "spkl_gi", title:"Tgl. GI", type: "text", width: 120, align:"center" },
            { name: "spkl_wcetak", title:"", type: "text", width: 70, align:"center" },
            { name: "spkl_cetak", title:"Tgl. Cetak", type: "text", width: 120, align:"center" },
            { name: "spkl_wtagihan", title:"", type: "text", width: 70, align:"center" },
            { name: "spkl_tagihan", title:"Tgl. Tagihan", type: "text", width: 120, align:"center" },
            { name: "spkl_wlunas", title:"", type: "text", width: 70, align:"center" },
            { name: "spkl_lunas", title:"Tgl. Lunas", type: "text", width: 120, align:"center" },
            { name: "spkl_wrefund", title:"", type: "text", width: 70, align:"center" },
            { name: "spkl_refund", title:"Tgl. Refund", type: "text", width: 120, align:"center" },
            { name: "spkl_jumlah_refund", title:"Jumlah Refund", type: "text", width: 120, align:"right" },
            { type: "control", editButton:false, deleteButton:false,  width:50, align:"center", itemTemplate: function(value, item) {
                        var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                        
                        var $customButton = $("<a href='javascript:;' class='red-text cetak' title='Cetak'><span class='material-icons'  style='font-size:20px'>print</span></a>")
                            .click(function(e) {
                                $(".spk").html(item.spk_id);
                                var print = "{{url('/')}}/cetak/leasing/";

                                $("#ctagihan").attr("href",print + "tagihan/" + item.spk_id);
                                $("#cdp").attr("href",print + "dp/" + item.spk_id);
                                $("#clunas").attr("href",print + "lunas/" + item.spk_id);


                                function cetak_save(id){
                                    var data = {
                                        "spkl_spk":id,
                                        _token:'{{csrf_token()}}'
                                    };
                                    $.ajax({
                                        type: "PUT",
                                        url: "{{url('/api/spk/leasing/cetak_save')}}",
                                        data: data
                                    }).fail(function(response){
                                        console.log(response);
                                    });
                                    loadData();
                                }

                                $("#ctagihan").click(function(){
                                    cetak_save(item.spk_id);
                                });
                                $("#clunas").click(function(){
                                    cetak_save(item.spk_id);
                                });
                                $("#cdp").click(function(){
                                    cetak_save(item.spk_id);
                                });

                                $("#cetak").modal("open");
                                e.stopPropagation();
                            });
                        
                        return $result.add($customButton);
                    }   }
        ]
    });
};loadData();


   function asuransi(e){
        var id = $(e).data('id');

        $(".spk").html(id);
        $.ajax({
            type: "GET",
            url: "{{url('api/spk/leasing/asuransi')}}/"
        }).done(function(data) {
            $(".asuransi").html("");
            for(var i in data){
                var item = data[i];
                $(".asuransi").append("<option value='"+item.asuransi_id+"'>"+item.asuransi_nama+"</option>");
            }
            $("#setasuransi").modal("open");
        });

    };

   function tagihan(e){
        var id = $(e).data('id');

        $(".spk").html(id);
        $("#tagihan").modal("open");
    };
   function lunas(e){
        var id = $(e).data('id');

        $(".spk").html(id);
        $("#lunas").modal("open");
    };
   function refund(e){
        var id = $(e).data('id');

        $(".spk").html(id);
        $("#refund").modal("open");
    };



   function asuransi_save(e){
        var id = $(".spk").html();
        var asuransi = $(".asuransi").val();
        var data = {
            "spkl_spk":id,
            "spkl_asuransi":asuransi,
            _token:'{{csrf_token()}}'
        };
         $.ajax({
            type: "PUT",
                url: "{{url('/api/spk/leasing/asuransi_save')}}",
                data: data
            }).fail(function(response){
                console.log(response);
            });
         loadData();
    };



   function tagihan_save(e){
        var id = $(".spk").html();
        var tgl = $(".tgl").val();
        var data = {
            "spkl_spk":id,
            "spkl_tagihan":tgl,
            _token:'{{csrf_token()}}'
        };
         $.ajax({
            type: "PUT",
                url: "{{url('/api/spk/leasing/tagihan_save')}}",
                data: data
            }).fail(function(response){
                console.log(response);
            });
         loadData();
    };


   function lunas_save(e){
        var id = $(".spk").html();
        var tgl = $(".tgl").val();
        var data = {
            "spkl_spk":id,
            "spkl_lunas":tgl,
            _token:'{{csrf_token()}}'
        };
         $.ajax({
            type: "PUT",
                url: "{{url('/api/spk/leasing/lunas_save')}}",
                data: data
            }).fail(function(response){
                console.log(response);
            });
         loadData();
    };


   function refund_save(e){
        var id = $(".spk").html();
        var tgl = $(".tgl").val();
        var jumlah = string_format($(".jumlah").val());

        var data = {
            "spkl_spk":id,
            "spkl_refund":tgl,
            "spkl_jumlah_refund":jumlah,
            _token:'{{csrf_token()}}'
        };
         $.ajax({
            type: "PUT",
                url: "{{url('/api/spk/leasing/refund_save')}}",
                data: data
            }).fail(function(response){
                console.log(response);
            });
         loadData();
    };

</script>

@endsection