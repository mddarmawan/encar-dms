@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            Data Aksesoris
        </h6>
        <ul class="header-tools right" style="margin-left: 20px">
            <li><a href="#vendorAks" class="chip"> Data Vendor Aksesoris</a></li>
        </ul>
    </div>

<div class="wrapper">
    <div id="dataAksesoris">

    </div>  
</div>

<div id="vendorAks" class="modal" style="width:1000px;">  
    <h6 class="modal-title blue-grey darken-1">
        Data Vendor Aksesoris
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        @include("modules.vendorAks.data")  
    </div>
</div>


<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    
<script>
      // Initialize Firebase
    var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
    };
        firebase.initializeApp(config);
        firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
            console.log(error.message);
    });
</script>


<script>
    function vendor() {
    $.ajax({
        type: "GET",
        url: "{{url('api/vendorAks')}}"
    }).done(function(vendorAks) {
        vendorAks.unshift({ vendorAks_id: "0", vendorAks_nama: "" });

    function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

        var db = {
           loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/aksesoris')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_aksesoris/" + item.aksesoris_id + "/");
                    db.set({
                        aksesoris_id: item.aksesoris_kode,
                        aksesoris_type: item.aksesoris_kendaraan,
                        aksesoris_nama : item.aksesoris_nama,
                        aksesoris_harga : item.aksesoris_harga
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_aksesoris/" + item.aksesoris_id + "/");
                    db.set({
                        aksesoris_id: item.aksesoris_kode,
                        aksesoris_type: item.aksesoris_kendaraan,
                        aksesoris_nama : item.aksesoris_nama,
                        aksesoris_harga : item.aksesoris_harga
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_aksesoris/" + item.aksesoris_id + "/").remove();                   
                });
            }
        };

   

    $("#dataAksesoris").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
                { name: "aksesoris_kode", title:"Kode Aksesoris", type: "text", width: 120, validate: "required", align:"center" },
                { name: "aksesoris_nama", title:"Nama Aksesoris", type: "text", width: 200, validate: "required", align:"center" },
                { name: "aksesoris_kendaraan", title:"Type Kendaraan", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left" },
                { name: "aksesoris_harga", title:"Harga", type: "number", width: 100, validate: "required" , align:"center"},
                { name: "aksesoris_vendor", title:"Vendor", type: "select", items: vendorAks, valueField: "vendorAks_id", textField: "vendorAks_nama", width: 100, align:"left", validate:  { message: "Vendor harus diisi !", validator: function(value) { return value > 0; } }},
                { type: "control", width:70 }
            ]
        });
      });
 
};

type();
    });
 
};
vendor();
</script>

@endsection