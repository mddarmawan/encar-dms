<div id="dataVendorAks">

</div>  

<script>
$(function() {

    var db = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/vendorAks')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/vendorAks')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/vendorAks')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/vendorAks')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };


    $("#dataVendorAks").jsGrid({
        height: "380px",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "vendorAks_nama", title:"Nama Vendor", type: "text", width: 150, validate: "required" },
            { name: "vendorAks_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "vendorAks_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "vendorAks_kodepos", title:"Kode Pos", type: "number", width: 120},
            { name: "vendorAks_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { name: "vendorAks_email", title:"Email", type: "text", width: 100 },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

