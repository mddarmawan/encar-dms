@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pembelian</small>
            Ekspedisi Kendaraan
        </h6>
    </div>

<div class="wrapper">
    <div id="dataEkspedisi">

    </div>  
</div>

<script>
$(function() {

    var db_ekspedisi = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/ekspedisi')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };

    $("#dataEkspedisi").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_ekspedisi,
 
        fields: [
            { name: "ekspedisi_nama", title:"Nama ekspedisi", type: "text", width: 150, validate: "required" },
            { name: "ekspedisi_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "ekspedisi_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "ekspedisi_kodepos", title:"Kode Pos", type: "number", width: 120, validate: "" },
            { name: "ekspedisi_telepon", title:"Telepon", type: "number", width: 100, validate: "required" },
            { name: "ekspedisi_email", title:"Email", type: "text", width: 100, validate: "" },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

@endsection