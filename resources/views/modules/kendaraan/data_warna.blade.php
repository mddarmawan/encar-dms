<div id="dataWarna">

</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
      };
      firebase.initializeApp(config);
      firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
          console.log(error.message);
        });
    </script>

<script>

function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

function warna() {

    var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/warna')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_warna/" + item.warna_id + "/");
                    db.set({
                        warna_id: item.warna_id,
                        warna_nama: item.warna_nama,
                        warna_type: item.warna_type,
                        warna_status: item.warna_status
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_warna/" + item.warna_id + "/");
                    db.set({
                        warna_id: item.warna_id,
                        warna_nama: item.warna_nama,
                        warna_type: item.warna_type,
                        warna_status: item.warna_status
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_warna/" + item.warna_id + "/").remove();                   
                });
            }
        };

        db.status = [{'status_id':'', 'status_nama':''},{'status_id':1, 'status_nama':'ACTIVE'}, {'status_id':0, 'status_nama':'NON ACTIVE'}]

        $("#dataWarna").jsGrid({
            height: "100%",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "warna_type", title:"Type", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left" },
                { name: "warna_nama", title:"nama", type: "text", width: 120, validate: "required" },
                { type: "control", width:70 }
            ]

        });

    
};
warna();

});
};
type();
</script>