<center>
<br>
<br><br>
<h2>STORE SPK</h2>
</center>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
	apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
	authDomain: "dealer-ms.firebaseapp.com",
	databaseURL: "https://dealer-ms.firebaseio.com",
	projectId: "dealer-ms",
	storageBucket: "dealer-ms.appspot.com",
	messagingSenderId: "950788435795"
  };
  firebase.initializeApp(config);
  firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
	  console.log(error.message);
	});
</script>
<script>

var firebaseDB = firebase.database();

//PARSING SPK & DISKON SALES
$.ajax({
	type: "GET",
	url: "{{url('api/firebase/sales')}}"
}).done(function(data){
	if (data){
		for(var i in data){
			var sales = data[i];
			parseSPKSales(sales.sales_salesUid);			
			parseDiskonSales(sales.sales_salesUid);			
		}
	}
}); 

function parseSPKSales(uid){
	var db = firebase.database().ref('sales/' + uid + '/spk/');
	db.on('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var item = childSnapshot.val();
			item['spk_id'] = childSnapshot.key;
			item['_token'] = '{{csrf_token()}}'

			if (item.spk_status != 99){
				$.ajax({
					type: "POST",
					url : "{{url('/api/firebase/spk')}}",
					data: item
				}).done(function(result){
					if (result){
						console.log(result);
						//db.child(childSnapshot.key).remove();
					}
				}); 
			}
		});
	});
}

function parseDiskonSales(uid){
	var db = firebaseDB.ref('sales/' + uid + '/diskon/');
	db.on('value', function(snapshot) {
		snapshot.forEach(function(childSnapshot) {
			var item = childSnapshot.val();
			item['_token'] = '{{csrf_token()}}';
			
			if (item.diskon_status != 9){
				$.ajax({
					type: "POST",
					url: "{{url('/api/firebase/diskon')}}",
					data: item
				}).done(function(result){
					if (result){
						console.log(result);
						//db.child(childSnapshot.key).remove();
					}
				}); 
			}
		});
	});
}
</script>