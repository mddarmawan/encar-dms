<div class="color-line">
    </div>
<nav class="nav-fluid ">
	<a href="{{url('/')}}" class="brand left">
		<img src="{{url('/assets/images/logo.png')}}"/>
	</a>
	<ul class="app-menu">
		<li class=""><a href="#ho">Penjualan</a> </li>
		<li><a href="#purchase">Pembelian</a> </li>
		<li><a href="#inventory">Persediaan</a> </li>
		<li><a href="#payroll">Penggajian</a> </li>
		<li><a href="#account">Keuangan</a> </li>
		<li><a href="#crm">CR<span class="underline">M</span></a></li>
		<li><a href="#contact">Daftar Kontak</a> </li>
		<li><a href="#kasir"><span class="underline">K</span>asir</a> </li>
		<li><a href="#report">Laporan</a> </li>
		<li><a href="#pengaturan">Pengaturan</a> </li>
	</ul>
	<ul class="app-tools right">
		<li><a href="#" class="waves-effect waves-light"><span class="fa fa-bell-o"></span></a></li>
		<li><a href="#" class="waves-effect waves-light"><span class="fa fa-user-o"></span></a></li>
	</ul>	

</nav>
<div class="nav-sub">
	<div id="ho">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/pelanggan')}}"><i class="icon-pelanggan"></i><span>Pelanggan</span></a></li>
				<li><a href="{{url('/spk')}}"><i class="icon-spk"></i><span>SPK</span></a></li>
				<li><a href="{{url('/match')}}"><i class="icon-matching"></i><span>Matching</span></a></li>
				<li><a href="{{url('/leasing')}}"><i class="icon-leashing"></i><span>Tagihan Leasing</span></a></li>
				<li><a href="{{url('/penjualan')}}"><i class="icon-penjualan"></i><span>Penjualan</span></a></li>
				<li><a href="{{url('/samsat')}}"><i class="icon-ttbj"></i><span>Proses SAMSAT</span></a></li>
			</ul>
			<h6>Penjualan Kendaraan</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/aksesoris')}}"><i class="icon-aksesoris"></i><span>Aksesoris</span></a></li>
				<li><a href="{{url('/reqdiskon')}}"><i class="icon-req-diskon"></i><span>Req. Diskon</span></a></li>
				<li><a href="{{url('/diskon')}}"><i class="icon-diskon"></i><span>Diskon</span></a></li>
				<li><a href="{{url('/referral')}}"><i class="icon-referral"></i><span>Referral</span></a></li>
			</ul>
			<h6>Diskon</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/bast')}}"><i class="icon-stock-unit"></i><span>BAST</span></a></li>
				<li><a href="{{url('/stockunit')}}"><i class="icon-stock-unit"></i><span>Stock Unit</span></a></li>
				<li><a href="{{url('/report/penjualan')}}"><i class="icon-penjualan-report"></i><span>Penjualan</span></a></li>
			</ul>
			<h6>Laporan</h6>
		</div>

		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/bbn')}}"><i class="icon-ttbj"></i><span>BBN</span></a></li>
			</ul>
			<h6>-</h6>
		</div>
	</div>
	<div id="purchase">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/vendor')}}"><i class="icon-vendor"></i><span>Vendor</span></a></li>
				<li><a href="{{url('/ekspedisi')}}"><i class="icon-agen-ekspedisi"></i><span>Ekspedisi</span></a></li>
				<li><a href="{{url('/reqkendaraan')}}"><i class="icon-req-kendaraan"></i><span>Req. Kendaraan</span></a></li>
				<li><a href="{{url('/pembelian')}}"><i class="icon-pembelian"></i><span>Pembelian</span></a></li>
				<li><a href="{{url('/poretur')}}"><i class="icon-retur-kendaraan"></i><span>Retur Kendaraan</span></a></li>
			</ul>
			<h6>Pembelian Kendaraan</h6>
		</div>
	</div>
	<div id="inventory">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/kendaraan')}}"><i class="icon-kendaraan"></i><span>Kendaraan</span></a></li>
				<li><a href="{{url('/gudang')}}"><i class="icon-gudang"></i><span>Gudang</span></a></li>
				<!--<li><a href="{{url('/stockin')}}"><i class="fa fa-tachometer"></i><span>Stock In</span></a></li>-->
			</ul>
			<h6>Persediaan</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/stock')}}"><i class="icon-stock-opname"></i><span>Stock</span></a></li>
				<li><a href="{{url('/stockmoves')}}"><i class="icon-perpindahan-stock"></i><span>Perpindahan Stok</span></a></li>
			</ul>
			<h6>Laporan</h6>
		</div>
	</div>
	<div id="payroll">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/payroll/karyawan')}}"><i class="icon-karyawan"></i><span>Karyawan</span></a></li>
				<li><a href="{{url('/payroll/sales')}}"><i class="icon-sales-n-team"></i><span>Sales &amp; Team</span></a></li>
			</ul>
			<h6>Data Karyawan</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<!--<li><a href="{{url('/payroll/absensi')}}"><i class="icon-absensi"></i><span>Absensi</span></a></li>-->
				<li><a href="{{url('/payroll/gaji')}}"><i class="icon-penggajian"></i><span>Penggajian</span></a></li>
				<li><a href="{{url('/payroll/insentif')}}"><i class="icon-sales-insentif"></i><span>Sales Insentif</span></a></li>
			</ul>
			<h6>Laporan</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/vendor')}}"><i class="icon-hari-libur"></i><span>Hari Libur</span></a></li>
				<li><a href="{{url('/ekspedisi')}}"><i class="icon-tunjangan"></i><span>Tunjangan</span></a></li>
				<li><a href="{{url('/poquote')}}"><i class="icon-grade-sales"></i><span>Grade Sales</span></a></li>
			</ul>
			<h6>Pengaturan</h6>
		</div>
	</div>
	<div id="account">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/akuntansi/akun')}}"><i class="icon-akun"></i><span>Akun</span></a></li>
				<li><a href="{{url('/akuntansi/bank')}}"><i class="icon-bank"></i><span>Bank</span></a></li>
			</ul>
			<h6>Pengaturan</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/akuntansi/ejurnal')}}"><i class="icon-jurnal"></i><span>Jurnal</span></a></li>
			</ul>
			<h6>Entry</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/akuntansi/bukubesar')}}"><i class="icon-buku-besar"></i><span>Buku Besar</span></a></li>
				<li><a href="{{url('/akuntansi/jurnal')}}"><i class="icon-jurnal-umum"></i><span>Jurnal Umum</span></a></li>
				<li><a href="{{url('/akuntansi/saldo')}}"><i class="icon-neraca-saldo"></i><span>Neraca Saldo</span></a></li>
				<li><a href="{{url('/akuntansi/neraca')}}"><i class="icon-neraca"></i><span>Neraca</span></a></li>
				<li><a href="{{url('/akuntansi/labarugi')}}"><i class="icon-laba-rugi"></i><span>Laba/Rugi</span></a></li>
			</ul>
			<h6>Laporan</h6>
		</div>
	</div>
	<div id="crm">
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/akuntansi/akun')}}"><i class="icon-customer-service"></i><span>CRC</span></a></li>
			</ul>
			<h6>CS</h6>
		</div>
		<div class="sub-menu">
			<ul>
				<li><a href="{{url('/akuntansi/ejurnal')}}"><i class="icon-jadwal-service"></i><span>Jadwal Service</span></a></li>
				<li><a href="{{url('/akuntansi/ejurnal')}}"><i class="icon-hari-raya"></i><span>Hari Raya</span></a></li>
				<li><a href="{{url('/akuntansi/ejurnal')}}"><i class="icon-ulang-tahun"></i><span>Ultah</span></a></li>
				<li><a href="{{url('/akuntansi/ejurnal')}}"><i class="icon-promosi"></i><span>Promo</span></a></li>
			</ul>
			<h6>Informasi</h6>
		</div>
	</div>
</div>