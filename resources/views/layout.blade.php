<!DOCTYPE html>
<html lang="en">
	<head>
		@include("layouts.head")
	</head>
	<body id="apps">
		@include("layouts.header")
		@include("layouts.sidebar")
		@include("layouts.error")
		@yield("content")


		@include("layouts.footer")
	</body>
</html>