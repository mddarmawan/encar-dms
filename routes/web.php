<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');


//-------------------------------//
// PENJUALAN
//-------------------------------//

Route::get('pelanggan', function () {
    return view('modules.pelanggan.data');
});

Route::get('spk/permintaan_spk', function () {
    return view('modules.spk.permintaan_spk');
});

Route::get('spk/permintaan_do', function () {
    return view('modules.spk.permintaan_do');
});
Route::get('spk', function () {
    return view('modules.spk.data');
});

Route::get('spk/cancel', function () {
    return view('modules.spk.cancel');
});

Route::get('aksesoris', function () {
    return view('modules.aksesoris.data');
});

Route::get('reqdiskon', function () {
    return view('modules.diskon.req');
});

Route::get('diskon', function () {
    return view('modules.diskon.data');
});

Route::get('pembelian', function () {
    return view('modules.pembelian.data');
});

Route::get('samsat', function () {
    return view('modules.samsat.data');
});


Route::get('samsat/riwayat', function () {
    return view('modules.samsat.riwayat');
});


//-------------------------------//
// PEMBELIAN
//-------------------------------//

Route::get('leasing', function () {
    return view('modules.leashing.data');
});

Route::get('vendor', function () {
    return view('modules.vendor.data');
});


//-------------------------------//
// PERSEDIAAN
//-------------------------------//

Route::get('kendaraan', function () {
    return view('modules.kendaraan.data');
});

Route::get('bbn', function () {
    return view('modules.bbn.data');
});

Route::get('gudang', function () {
    return view('modules.gudang.data');
});

Route::get('/import', function () {
    return view('import');
});


//-------------------------------//
//pengaturan spk
//-------------------------------//
Route::get('spk/no_spk', function () {
    return view('modules.spk.data_nospk');
});
 

//----------------------------------------------------------//

// DATA API
//----------------------------------------------------------//


//-------------------------------//
//Leasing
//-------------------------------//
Route::get('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@index']);
Route::post('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@store']);
Route::put('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@update']);
Route::delete('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@destroy']);


//-------------------------------//
// Aksesoris
//-------------------------------//
Route::get('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@index']);
Route::post('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@store']);
Route::put('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@update']);
Route::delete('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@destroy']);


//-------------------------------//
// ekspedisi
//-------------------------------//
Route::get('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@index']);
Route::post('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@store']);
Route::put('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@update']);
Route::delete('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@destroy']);


//-------------------------------//
// CMO
//-------------------------------//
Route::get('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@index']);
Route::post('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@store']);
Route::put('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@update']);
Route::delete('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@destroy']);


//-------------------------------//
// Asuransi
//-------------------------------//
Route::get('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@asuransi_read']);
Route::post('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@add_asuransi']);
Route::put('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@update_asuransi']);
Route::delete('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@destroy_asuransi']);

//-------------------------------//
// BIRO
//-------------------------------//
Route::get('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@index']);
Route::post('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@store']);
Route::put('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@update']);
Route::delete('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@destroy']);


//-------------------------------//
//Gudang
//-------------------------------//
Route::get('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@index']);
Route::post('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@store']);
Route::put('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@update']);
Route::delete('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@destroy']);


//-------------------------------//
//Master Kendaraan
//-------------------------------//
Route::get('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);
Route::post('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_store']);
Route::put('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_update']);
Route::delete('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_destroy']);
Route::get('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_read']);
Route::get('api/kendaraan/variant/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_type']);
Route::post('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_store']);
Route::put('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_update']);
Route::delete('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_destroy']);
Route::get('api/kendaraan/type/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);


//-------------------------------//
//Master BBN
//-------------------------------//
Route::get('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_master']);
Route::put('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_update']);


//-------------------------------//
//Warna
//-------------------------------//
Route::get('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@index']);
Route::get('api/warna/variant', ['middleware' => 'cors','uses'=>'ApiWarnaController@warna_variant']);
Route::post('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@store']);
Route::put('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@update']);
Route::delete('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@destroy']);


//-------------------------------//
//VendorAks
//-------------------------------//
Route::get('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@index']);
Route::post('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@store']);
Route::put('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@update']);
Route::delete('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@destroy']);


//-------------------------------//
//Pelanggan
//-------------------------------//
Route::get('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@index']);
Route::delete('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@destroy']);
Route::get('api/pelanggan/{id}', ['middleware' => 'cors','uses'=>'ApiPelangganController@detail']);

//-------------------------------//
//SPK
//-------------------------------//
Route::post('api/spk/variant_update', ['middleware' => 'cors','uses'=>'ApiSPKController@variant_update']);
Route::post('api/spk/leasing_save', ['middleware' => 'cors','uses'=>'ApiSPKController@leasing_save']);
Route::post('api/spk/acc_pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@acc_pengajuan_do']);
Route::post('api/spk/tolak_pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@tolak_pengajuan_do']);
Route::post('api/spk/pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@pengajuan_do']);
Route::post('api/spk/terbitkan', ['middleware' => 'cors','uses'=>'ApiSPKController@terbitkan']);
Route::post('api/spk/acc_permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@acc_permintaan_spk']);
Route::post('api/spk/tanggapi_permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@tanggapi_permintaan_spk']);
Route::get('api/spk', ['middleware' => 'cors','uses'=>'ApiSPKController@index']);
Route::get('api/spk/permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@permintaan_spk']);
Route::get('api/spk/permintaan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@permintaan_do']);
Route::get('api/spk/cancel', ['middleware' => 'cors','uses'=>'ApiSPKController@cancel']);
Route::post('api/spk/cancel', ['middleware' => 'cors','uses'=>'ApiSPKController@cancel_save']);
Route::get('api/spk/{id}', ['middleware' => 'cors','uses'=>'ApiSPKController@detail']);


//-------------------------------//
//karyawan
//-------------------------------//
Route::get('api/karyawan/sales', ['middleware' => 'cors','uses'=>'ApiKaryawanController@sales_view']);


//-------------------------------//
//no_spk
//-------------------------------//
Route::get('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@index']);
Route::get('api/pengaturan/lastno', ['middleware' => 'cors','uses'=>'ApiNospkController@lastNo']);
Route::post('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@store']);
Route::put('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@update']);
Route::delete('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@destroy']);


//-------------------------------//
//Referral
//-------------------------------//
Route::get('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@index']);
Route::post('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@store']);
Route::put('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@update']);
Route::delete('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@destroy']);


//-------------------------------//
//Diskon
//-------------------------------//
Route::get('api/diskon', ['middleware' => 'cors','uses'=>'ApiDiskonController@index']);
Route::get('api/diskon/aksesoris/{type}', ['middleware' => 'cors','uses'=>'ApiDiskonController@aksesoris']);
Route::get('api/diskon/request', ['middleware' => 'cors','uses'=>'ApiDiskonController@req']);
Route::get('api/diskon/request/{id}', ['middleware' => 'cors','uses'=>'ApiDiskonController@req']);
Route::get('api/diskon/referral', ['middleware' => 'cors','uses'=>'ApiDiskonController@referral']);
Route::get('api/diskon/riwayat', ['middleware' => 'cors','uses'=>'ApiDiskonController@riwayat_referral']);
Route::get('api/diskon/status', ['middleware' => 'cors','uses'=>'ApiDiskonController@status']);
Route::put('api/diskon/app', ['middleware' => 'cors','uses'=>'ApiDiskonController@app']);
Route::post('api/diskon/konfirmasi', ['middleware' => 'cors','uses'=>'ApiDiskonController@konfirmasi']);


//-------------------------------//
//Pembelian
//-------------------------------//
Route::get('api/pembelian/new', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@baru']);
Route::get('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@index']);
Route::post('api/pembelian/import', 'ApiTrKendaraanController@import');
Route::post('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@store']);
Route::put('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@update']);
Route::delete('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@destroy']);


//-------------------------------//
//Vendor
//-------------------------------//
Route::get('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@index']);
Route::post('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@store']);
Route::put('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@update']);
Route::delete('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@destroy']);

//-------------------------------//
//Cetak
//-------------------------------//
Route::get('cetak/{aksi}/{id}', 'CetakController@cetak');
Route::get('cetak', 'CetakController@index');


Route::get('/store/spk', function () {
    return view('store_spk');
});
Route::get('/store/diskon', function () {
    return view('store_diskon');
});
Route::get('api/firebase/sales', ['middleware' => 'cors','uses'=>'ApiFirebaseController@get_sales']);
Route::post('api/firebase/spk', ['middleware' => 'cors','uses'=>'ApiFirebaseController@storeSPK']);
Route::post('api/firebase/diskon', ['middleware' => 'cors','uses'=>'ApiFirebaseController@storeDiskon']);