<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use DateTime;

class ApiFirebaseController extends Controller
{
	public function get_sales($id = null){
		if (is_null($id)){
			$data = DB::table('tb_sales')->select("sales_id","sales_salesUid","team_nama","karyawan_nama")
				->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
				->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
				->whereNotNull("sales_salesUid")
				->where("sales_salesUid","!=","")
				->get();
		}else{
			$data = DB::table('tb_sales')->select("sales_id","sales_salesUid","team_nama","karyawan_nama")
				->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
				->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
				->whereNotNull("sales_salesUid")
				->where("sales_salesUid","!=","")
				->first();
		}
		
		if (count($data)>0){
			return json_encode($data);
		}
		return FALSE;		
	}

	public function storeSPK() {
		$proses = 0;
		$item = $_POST;
		unset($item['_token']);

		$foldername = 'data/identitas/';
		$spk_pel_fotoid = md5(date('Y-m-d H:i:s')) . '.jpg';
		$spk_stnk_fotoid = md5(date('Y-m-d H:i:s')) . '.jpg';

		$source = $item['spk_pel_lahir'];
		$date = DateTime::createFromFormat('d/m/Y', $source);

		Storage::append($foldername . $spk_pel_fotoid, base64_decode($item['spk_pel_fotoid']));
		Storage::append($foldername . $spk_stnk_fotoid, base64_decode($item['spk_stnk_fotoid']));

		foreach ($item as $key => $value) {
			if (strpos($key, '_pel_') !== false) {
				$column = str_replace("spk_", "", $key);
				$pelanggan[$column] = $value;
			}
		}

		$item['spk_pel_fotoid'] = $spk_pel_fotoid;
		$item['spk_stnk_fotoid'] = $spk_stnk_fotoid;
		$item['spk_pel_lahir'] = $date->format('Y-m-d');
		$pelanggan['pel_fotoid'] = $spk_pel_fotoid;
		$pelanggan['pel_lahir'] = $date->format('Y-m-d');

		$result = DB::table('tb_pelanggan')->where("pel_identitas", $item["spk_pel_identitas"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_pelanggan')->insert($pelanggan);
		} else {
			$insert = DB::table('tb_pelanggan')->where("pel_identitas", $item["spk_pel_identitas"])->update($pelanggan);
		}

		$result =  DB::table('tb_spk')->where("spk_id", $item["spk_id"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_spk')->insert($item);
		} else {
			$insert = DB::table('tb_spk')-> where('spk_id', request('spk_id'))->update($item);
		}

		$result =  DB::table('tb_spk')->where("spk_id", $item["spk_id"])->get();
		if (count($result) > 0){
			// $proses = 1;
			return "BERHASIL!";
		} else {
			// $proses = 0;
			// return $insert;
			return "GAGAL!!";
		}

		return $proses;
	}

	public function storeDiskon() {
		$proses = 0;
		$item = $_POST;

		foreach ($item['aksesoris'] as $key => $value) {
			$allAksesoris[$key] = $value;
		}

		unset($item['_token']);
		unset($item['aksesoris']);
		
		foreach ($item as $key => $value) {
			$column = str_replace("diskon_", "spkd_", $key);
			$diskon[$column] = $value;
		}

		$diskon['spkd_komisi'] = (int) $diskon['spkd_komisi'];

		$result =  DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_spk_diskon')->insert($diskon);
		} else {
			$insert = DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->update($diskon);
		}

		$diskon_id = DB::table('tb_spk_diskon')->where("spkd_spk", $diskon["spkd_spk"])->first();

		foreach ($allAksesoris as $a) {
			$aksesoris = array(
				"spka_diskon"	=> $diskon_id->spkd_id,
				"spka_kode"		=> $a['diskona_aksesoris'],
				"spka_nama"		=> $a['diskona_nama'],
				"spka_harga"	=> $a['diskona_harga'],
				"created_at"	=> date('Y-m-d H:i:s'),
				"updated_at"	=> date('Y-m-d H:i:s')
			);

			$result =  DB::table('tb_spk_aksesoris')
				->where("spka_kode", $a['diskona_aksesoris'])
				->where("spka_diskon", $diskon_id->spkd_id)
				->get();
			if (count($result) == 0){
				$insert = DB::table('tb_spk_aksesoris')->insert($aksesoris);
			} else {
				$insert = DB::table('tb_spk_aksesoris')
				->where("spka_kode", $a['diskona_aksesoris'])
				->where("spka_diskon", $diskon_id->spkd_id)
				->update($aksesoris);
			}
		}

		$result =  DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->get();
		if (count($result) > 0){
			// $proses = 1;
			return "BERHASIL!";
		} else {
			// $proses = 0;
			// return $insert;
			return "GAGAL!!";
		}

		return $proses;
	}

	function sales_input(){
		$validator = Validator::make(request()->all(), [
			"sales_Uid"		=> "required",
			"sales_id"		=> "required",
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] =  $validator->messages();

		} else {
			if (DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->first()) {
				$proses['result'] = DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->update([
					"sales_id"	=>  request("akun_id"),
					"sales_usr"	=>  request("akun_nama"),
				]);

				$proses['result'] = 1;  
			} else {
				$proses['result'] = DB::table('tb_sales')->insert([
					"sales_Uid"	 =>  request("sales_Uid"),
					"sales_id"	=>  request("akun_id"),
					"sales_usr"	=>  request("akun_nama"),
				]); 

				$proses['result'] = 1;	   
			}

		}
		
		return json_encode($proses);
	}

	 function salesUid_input(){
		$this->validate(request(), [
			"salesUid_id"		=> "required",
			"salesUid_spk"		=> "required",
			"salesUid_spkd"		=> "required",
		]);

		
		$insert = array(
			"salesUid_id"	 =>  request("salesUid_id"),
			"salesUid_spk"	 =>  request("salesUid_spk"),
			"salesUid_spkd"   =>  request("salesUid_spkd")
			);

		$id = DB::table('tb_sales_Uid')->insertGetId($insert, 'salesUid_id');
		return json_encode(DB::table('tb_sales_Uid')->where("salesUid_id", $id)->first());
	}

	function app(){
		$this->validate(request(), [
			"diskon_id"	  	=> "required",
			"diskon_status"	  => "required"
		]);

		
		$insert = array(
			 "spkd_cashback"	 	=>  request("diskon_cashback"),
			"spkd_komisi"	 		=>  request("diskon_komisi"),
			"spkd_status"	 		=>  request("diskon_status"),
			"spkd_catatan"	 		=>  request("diskon_catatan"),
		);

		$id = DB::table('tb_spk_aksesoris')-> insertGetId($insert,'diskon_id');
		return json_encode(DB::table('tb_spk_aksesoris') -> where("diskon_id", $id)->first());

	 }
}

