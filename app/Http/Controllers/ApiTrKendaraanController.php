<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Excel;

class ApiTrKendaraanController extends Controller
{
    function index(){
    	$trKendaraan = DB::table('tb_tr_kendaraan')->get();
		$result = $trKendaraan->filter(function ($trKendaraan) {
		    return 
		    	(!request("trk_ref") || strrpos(strtolower($trKendaraan->trk_ref), strtolower(request("trk_ref"))) > -1) &&
				 (!request("trk_invoice") || strrpos(strtolower($trKendaraan->trk_invoice), strtolower(request("trk_invoice"))) > -1) &&
                 (!request("trk_vendor") || strrpos(strtolower($trKendaraan->vendor->vendor_nama), strtolower(request("trk_vendor"))) > -1) &&
				 (!request("trk_tgl") || strrpos(strtolower($trKendaraan->trk_tgl), strtolower(request("trk_tgl"))) > -1) &&
				 (!request("trk_dh") || strrpos(strtolower($trKendaraan->trk_dh), strtolower(request("trk_dh"))) > -1) &&
				 (!request("trk_rrn") || strrpos(strtolower($trKendaraan->trk_rrn), strtolower(request("trk_rrn"))) > -1)&&
				 (!request("trk_variant") || strrpos(strtolower($trKendaraan->variant->variant_nama), strtolower(request("trk_variant"))) > -1)&&
				 (!request("trk_warna") || strrpos(strtolower($trKendaraan->warna->warna_nama), strtolower(request("trk_warna"))) > -1)&&
				 (!request("trk_harga") || strrpos(strtolower($trKendaraan->trk_dpp), strtolower(request("trk_harga"))) > -1);
		});

        $data = array();
        foreach($result as $r){
            $vendor = DB::table('tb_vendor')->where('vendor_id', $r->trk_vendor)->first();
            $variants = DB::table('tb_variant')
                ->leftjoin('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
                ->where('variant_id', $r->trk_variantid)
                ->get();
            $warna = DB::table('tb_warna')->where('warna_id', $r->trk_warna)->first();

            foreach ($variants as $v) {
                $variant = $v;
            }

            $item = array();
            $item['trk_id'] = $r->trk_id;
            $item['trk_ref'] = $r->trk_ref;
            $item['trk_tgl'] = date_format(date_create($r->trk_tgl),"d/m/Y");
            $item['trk_invoice'] = $r->trk_invoice;
            $item['trk_vendor'] = (count($vendor) > 0 ? $vendor->vendor_nama : NULL);
            $item['trk_variant'] = (count($variants) > 0 ? $variant->type_nama . " " . $variant->variant_nama : NULL);

            $item['trk_warna'] = ($warna != NULL ? $warna->warna_nama : 'BEBAS');
            $item['trk_dh'] = $r->trk_dh;
            $item['trk_rrn'] = $r->trk_rrn;
            $item['trk_harga'] = number_format($r->trk_hrg_dpp,0,",",".");
            $item['trk_mesin'] = $r->trk_mesin;
            $item['trk_rangka'] = $r->trk_rangka;
            $item['trk_vendorid'] = $r->trk_vendor;
            $item['trk_variantid'] = $r->trk_variantid;
            $item['trk_type'] = (count($variants) > 0 ? $variant->variant_type : NULL);
            $item['trk_warnaid'] = $r->trk_warna;
            $item['trk_tahun'] = $r->trk_tahun;
            $item['trk_dpp'] = $r->trk_hrg_dpp;
            $item['detail'] = "<a href='#detail' class='green-text detail' onclick='detail(this)'  data-id='".$r->trk_ref."'  title='Detail'><span class='material-icons'  style='font-size:20px'>done</span></a>";
            array_push($data, $item);
        }

    	return json_encode($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function import()
    {
        if (!empty(request('import_file'))) { 
            $path = request('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();

            if(!empty($data) && $data->count()){
                $tr_kendaraan = DB::table('tb_tr_kendaraan')->orderBy('trk_id', 'desc')->first();
                $no = ($tr_kendaraan != NULL ? $tr_kendaraan->trk_id : NULL);

                $no_faktur = 'no._faktur';
                $tgl_faktur = 'tgl._faktur';
                $no_mesin = 'no._mesin';

                foreach ($data as $key => $value) {
                    $no++;
                    $vendor = DB::table('tb_vendor')->where('vendor_nama', $value->branch_description)->first();
                    $variant = DB::table('tb_variant')->where('variant_serial', $value->tipe_kendaraan)->first();
                    $warna = DB::table('tb_warna')->where('warna_nama', $value->color_description)->first();

                    $insert[] = [
                        'trk_ref' => "P-" . sprintf("%05s", $no),
                        'trk_vendor' => ($vendor != NULL ? $vendor->vendor_id : NULL),
                        'trk_invoice' => $value->$no_faktur,
                        'trk_tgl' => $value->$tgl_faktur,
                        'trk_dh' => '2017000'.$no,
                        'trk_variantid' => ($variant != NULL ? $variant->variant_id : NULL),
                        'trk_warna' => ($warna != NULL ? $warna->warna_id : NULL),
                        'trk_tahun' => $value->construction_year,
                        'trk_rangka' => $value->chasis,
                        'trk_mesin' => $value->$no_mesin,
                        'trk_hrg_unit' => $value->harga_unit,
                        'trk_hrg_diskon' => $value->harga_discount,
                        'trk_hrg_pbm' => $value->harga_pbm,
                        'trk_hrg_dpp' => $value->harga_dpp,
                        'trk_hrg_ppn' => $value->harga_ppn,
                        'trk_hrg_total' => $value->harga_total_ar,
                        'trk_tgl_tempo' => $value->tanggal_jatuh_tempo,
                        'trk_interest' => $value->interest,
                        'trk_promes' => $value->promes,
                        'trk_tgl_fisik' => $value->grfisikdate,
                        'created_at' => date('Y-m-d h:i:s')
                        // '' => $value->company_code,
                        // '' => $value->branch_code,
                        // '' => $value->branch_description,
                        // '' => $value->filling_number,
                        // '' => $value->,
                        // '' => $value->tipe_kendaraan,
                        // '' => $value->color_description,
                        // 'trk_mesin' => $value->,
                    ];
                }

                if(!empty($insert)){
                    DB::table('tb_tr_kendaraan')->insert($insert);
                    return redirect('/pembelian');
                }
            }

            return redirect('/pembelian');
        }
    }

    function baru(){
        $data = DB::table("tb_tr_kendaraan") ->orderBy('trk_dh', 'desc')->first();
        if (is_null($data)){
            $result['dh'] = date("Y")."0001";
            $result['ref'] = "P-00001";
        }else{
            $result['dh'] = $data->trk_dh + 1;

            $no = substr($data->trk_ref,2,strlen($data->trk_ref)) + 1;
            if (strlen($no)==1){
                $no = "0000".$no;
            } else if (strlen($no)==2){
                $no = "000".$no;
            } else if (strlen($no)==3){
                $no = "00".$no;
            } else if (strlen($no)==4){
                $no = "0".$no;
            }
            $result['ref'] = "P-".$no;

        }
        return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "trk_ref"      => "required",
            "trk_invoice"      => "required",
            "trk_tgl"     => "required",
            "trk_dh"     => "required",
            "trk_rrn"     => "required",
            "trk_vendor"     => "required",
            "trk_variantid"     => "required",
            "trk_warna"     => "required",
            "trk_tahun"     => "required",
            "trk_rangka"     => "required",
            "trk_mesin"     => "required",
            "trk_dpp"     => "required",
        ]);


        $insert = TrKendaraan::create([
            "trk_ref"     =>  request("trk_ref"),
            "trk_invoice"     =>  request("trk_invoice"),
            "trk_tgl"     =>  date_format(date_create(str_replace("/","-",request("trk_tgl"))),"Y-m-d"),
            "trk_dh"   =>  request("trk_dh"),
            "trk_vendor"     =>  request("trk_vendor"),
            "trk_rrn"     =>  request("trk_rrn"),
            "trk_variantid"     =>  request("trk_variantid"),
            "trk_warna"     =>  request("trk_warna"),
            "trk_tahun"     =>  request("trk_tahun"),
            "trk_rangka"   =>  request("trk_rangka"),
            "trk_mesin"     =>  request("trk_mesin"),
            "trk_dpp"     =>  request("trk_dpp")
        ]);
        $result =  TrKendaraan::where("trk_id",$insert->id)->first();

        if (count($result)>0){
            return 1;
        }
        return 0;
    }

    function update(){
    	$this->validate(request(), [
            "trk_ref"      => "required",
            "trk_invoice"      => "required",
            "trk_tgl"     => "required",
            "trk_dh"     => "required",
            "trk_rrn"     => "required",
            "trk_vendor"     => "required",
            "trk_variantid"     => "required",
            "trk_warna"     => "required",
            "trk_tahun"     => "required",
            "trk_rangka"     => "required",
            "trk_mesin"     => "required",
            "trk_dpp"     => "required",
        ]);

	    TrKendaraan::where("trk_id",request("trk_id"))->update([
            "trk_ref"     =>  request("trk_ref"),
            "trk_invoice"     =>  request("trk_invoice"),
            "trk_tgl"     =>  date_format(date_create(str_replace("/","-",request("trk_tgl"))),"Y-m-d"),
            "trk_dh"   =>  request("trk_dh"),
            "trk_vendor"     =>  request("trk_vendor"),
            "trk_rrn"     =>  request("trk_rrn"),
            "trk_variantid"     =>  request("trk_variantid"),
            "trk_warna"     =>  request("trk_warna"),
            "trk_tahun"     =>  request("trk_tahun"),
            "trk_rangka"   =>  request("trk_rangka"),
            "trk_mesin"     =>  request("trk_mesin"),
            "trk_dpp"     =>  request("trk_dpp")
	    ]);

	   $result =  TrKendaraan::where("trk_id",request("trk_id"))->first();

        if (count($result)>0){
            return 1;
        }
        return 0;
    }

    function destroy(){
		$result = TrKendaraan::where('trk_id', request("trk_id"))->delete();
        if ($result){
            return 1;
        }
        return 0;
    }
	
	function request_kendaraan(){
		$data = DB::table('tb_spk')
    		->join('vw_stock_kendaraan', function($join){
				$join->on('vw_stock_kendaraan.trk_variantid','!=',DB::raw('tb_spk.spk_kendaraan and vw_stock_kendaraan.trk_warna != tb_spk.spk_warna '));
			})
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_variant', 'tb_variant.variant_id', '=', 'tb_spk.spk_kendaraan')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->join('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
    		->where("spk_dh",NULL)
    		->where("spk_do",NULL)
			->select("tb_spk.*","tb_warna.*","tb_variant.*","tb_type.*", "tb_karyawan.*","tb_team.*")->distinct()
    		->get();

		return $data;
	}
	
	function stock(){
		$data = DB::table('tb_tr_kendaraan')
    		->join('tb_variant', 'tb_variant.variant_id', '=', 'tb_tr_kendaraan.trk_variantid')
    		->join('tb_type', 'type_id', '=', 'variant_type')
    		->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
    		->leftjoin('tb_ekspedisi', 'tb_ekspedisi.ekspedisi_id', '=', 'tb_tr_kendaraan.trk_ekspedisi')
    		->leftjoin('tb_gudang', 'tb_gudang.gudang_id', '=', 'tb_tr_kendaraan.trk_lokasi')
    		->leftjoin('tb_spk', 'tb_spk.spk_dh', '=', 'tb_tr_kendaraan.trk_dh')
			->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("trk_invoice") || strrpos(strtolower($data->trk_invoice), strtolower(request("trk_invoice"))) > -1) && (!request("trk_tahun") || strrpos(strtolower($data->trk_tahun), strtolower(request("trk_tahun"))) > -1) && (!request("trk_dh") || strrpos(strtolower($data->trk_dh), strtolower(request("trk_dh"))) > -1) && (!request("trk_mesin") || strrpos(strtolower($data->trk_mesin), strtolower(request("trk_mesin"))) > -1) && (!request("trk_rangka") || strrpos(strtolower($data->trk_rangka), strtolower(request("trk_rangka"))) > -1) && (!request("variant_serial") || strrpos(strtolower($data->variant_serial), strtolower(request("variant_serial"))) > -1) && (!request("variant_nama") || strrpos(strtolower($data->variant_nama), strtolower(request("variant_nama"))) > -1) &&
				(!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1) &&
				(!request("ekspedisi_nama") || strrpos(strtolower($data->ekspedisi_nama), strtolower(request("ekspedisi_nama"))) > -1);
		});
		$data = array();
        foreach($result as $r){
            $item = array();
            $item['trk_id'] = $r->trk_id;
            $item['trk_invoice'] = $r->trk_invoice;
            $item['trk_tahun'] = $r->trk_tahun;
            $item['trk_tgl'] = date_format(date_create($r->trk_tgl),"d/m/Y");
            $item['trk_dh'] = $r->trk_dh;
            $item['trk_rangka'] = $r->trk_rangka;
            $item['trk_mesin'] = $r->trk_mesin;
            $item['variant_serial'] = $r->variant_serial;
            $item['variant_nama'] = $r->type_nama." ".$r->variant_nama;
            $item['warna_nama'] = $r->warna_nama;
            $item['ekspedisi_nama'] = $r->ekspedisi_nama;
			$item['trk_masuk'] = "";
			$item['trk_stock'] = "";
			$item['trk_status'] = "STOCK";
			$item['trk_lokasi'] = "INTRANSIT";
			if (!is_null($r->trk_masuk)){
				$item['trk_masuk'] = date_format(date_create($r->trk_masuk),"d/m/Y");
				$item['trk_stock'] = date_format(date_create($r->trk_masuk),"mY");
				$item['trk_lokasi'] = $r->gudang_nama;				
			}
			
			$item['spk_do'] = "";
			if (!is_null($r->spk_do)){
				$item['spk_do'] = date_format(date_create($r->spk_do),"d/m/Y");
				$item['trk_status'] = "DO";		
				$item['trk_lokasi'] = "KONSUMEN";			
			}
			if ((!request("trk_tgl") || strrpos(strtolower($item['trk_tgl']), strtolower(request("trk_tgl"))) > -1) &&
				 (!request("trk_masuk") || strrpos(strtolower($item['trk_masuk']), strtolower(request("trk_masuk"))) > -1) && 
				 (!request("trk_stock") || strrpos(strtolower($item['trk_stock']), strtolower(request("trk_stock"))) > -1) && 
				 (!request("trk_status") || strrpos(strtolower($item['trk_status']), strtolower(request("trk_status"))) > -1) && 
				 (!request("trk_lokasi") || strrpos(strtolower($item['trk_lokasi']), strtolower(request("trk_lokasi"))) > -1) &&
				 (!request("spk_do") || strrpos(strtolower($item['spk_do']), strtolower(request("spk_do"))) > -1) 			 
				){			
				array_push($data,$item);
			}
		}
		
		return json($data);
	}
	
	
}
