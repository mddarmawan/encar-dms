<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Referral;

class ApiReferralController extends Controller
{
     function index(){
    	$data = DB::table('tb_referral')-> get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("referral_nama") || strrpos(strtolower($data->referral_nama), strtolower(request("referral_nama"))) > -1) &&
				 (!request("referral_alamat") || strrpos(strtolower($data->referral_alamat), strtolower(request("referral_alamat"))) > -1) &&
				 (!request("referral_telp") || strrpos(strtolower($data->referral_telp), strtolower(request("referral_telp"))) > -1) &&
				 (!request("referral_bank") || strrpos(strtolower($data->referral_bank), strtolower(request("referral_bank"))) > -1) &&
				 (!request("referral_rek") || strrpos(strtolower($data->referral_rek), strtolower(request("referral_rek"))) > -1) &&
				 (!request("referral_an") || strrpos(strtolower($data->referral_an), strtolower(request("referral_an"))) > -1);
		});

    	return json_decode($result);
    }

    function store(){
    	$this->validate(request(), [
            "referral_nama"     	=> "required",
            "referral_telp"     	=> "required",
            "referral_bank"    	 	=> "required",
            "referral_rek"     		=> "required",
            "referral_an"     		=> "required"
        ]);

        
        $insert = array(
            "referral_nama"     =>  request("referral_nama"),
            "referral_alamat"     =>  request("referral_alamat"),
            "referral_telp"           =>  request("referral_telp"),
            "referral_bank"           =>  request("referral_bank"),
            "referral_rek"          =>  request("referral_rek"),
            "referral_an"          =>  request("referral_an")
        );

        $id = DB::table('tb_referral')->insertGetId($insert, 'referral_id');
        return json_encode(DB::table('tb_referral')->where("referral_id", $id)->first());
    }

    function update(){
    	$this->validate(request(), [
           	 "referral_nama"     	=> "required",
            "referral_telp"     	=> "required",
            "referral_bank"    	 	=> "required",
            "referral_rek"     		=> "required",
            "referral_an"     		=> "required"
        ]);

	    DB::table('tb_referral')-> where("referral_id",request("referral_id"))->update([
	        "referral_nama"     =>  request("referral_nama"),
            "referral_alamat"     =>  request("referral_alamat"),
            "referral_telp"           =>  request("referral_telp"),
            "referral_bank"           =>  request("referral_bank"),
            "referral_rek"          =>  request("referral_rek"),
            "referral_an"          =>  request("referral_an")
	    ]);

	    return json_encode(DB::table('tb_referral')-> where("referral_id",request("referral_id"))->first());
    }

    function destroy(){
		return DB::table('tb_referral')-> where('referral_id', request("referral_id"))->delete();
    }    
}
