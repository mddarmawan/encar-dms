<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Leasing;
use App\SPK_Leasing;

class ApiLeasingController extends Controller
{
    function index(){
    	$data = DB::table('tb_leasing')-> where('leasing_status',1)->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("leasing_nama") || strrpos(strtolower($data->leasing_nama), strtolower(request("leasing_nama"))) > -1) &&
				 (!request("leasing_nick") || strrpos(strtolower($data->leasing_nick), strtolower(request("leasing_nick"))) > -1) &&
				 (!request("leasing_alamat") || strrpos(strtolower($data->leasing_alamat), strtolower(request("leasing_alamat"))) > -1) &&
				 (!request("leasing_kota") || strrpos(strtolower($data->leasing_kota), strtolower(request("leasing_kota"))) > -1) &&
				 (!request("leasing_telp") || strrpos(strtolower($data->leasing_telp), strtolower(request("leasing_telp"))) > -1);
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "leasing_nama"      => "required",
            "leasing_nick"      => "required",
            "leasing_kota"     => "required",
            "leasing_telp"     => "required"
        ]);

        
        $insert = array(
            "leasing_nama"     =>  request("leasing_nama"),
            "leasing_nick"     =>  request("leasing_nick"),
            "leasing_kota"           =>  request("leasing_kota"),
            "leasing_alamat"           =>  request("leasing_alamat"),
            "leasing_telp"          =>  request("leasing_telp")
        );

        $id = DB::table('tb_leasing')-> insertGetId($insert,'leasing_id');
        return json_encode(DB::table('tb_leasing') -> where("leasing_id", $id)->first());
    }


    function update(){
    	$this->validate(request(), [
            "leasing_nama"      => "required",
            "leasing_nick"      => "required",
            "leasing_kota"     => "required",
            "leasing_telp"     => "required"
        ]);

	    DB::table('tb_leasing')->where("leasing_id",request("leasing_id"))->update([
	        "leasing_nama"     		=>  request("leasing_nama"),
            "leasing_nick"     		=>  request("leasing_nick"),
            "leasing_kota"          =>  request("leasing_kota"),
            "leasing_alamat"        =>  request("leasing_alamat"),
            "leasing_telp"          =>  request("leasing_telp")
	    ]);

	    return json_encode(DB::table('tb_leasing')-> where("leasing_id",request("leasing_id"))->first());
    }


    function destroy(){
		DB::table('tb_leasing')-> where("leasing_id",request("leasing_id"))->update(["leasing_status"=> 0 ]);


        return json_encode(DB::table('tb_leasing')-> where("leasing_id",request("leasing_id"))->first());        //return Leasing::where('leasing_id', request("leasing_id"))->delete();
    }



}
