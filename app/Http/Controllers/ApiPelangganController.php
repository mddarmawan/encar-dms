<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Pelanggan;

class ApiPelangganController extends Controller
{
    function index(){
    	$data = DB::table('tb_pelanggan')
                -> leftjoin('tb_sales','tb_pelanggan.pel_sales', '=','tb_sales.sales_id')
                -> leftjoin('tb_karyawan','tb_sales.sales_karyawan', '=','tb_karyawan.karyawan_id')
                -> get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("pel_nama") || strrpos(strtolower($data->pel_nama), strtolower(request("pel_nama"))) > -1) &&
				(!request("pel_alamat") || strrpos(strtolower($data->pel_alamat), strtolower(request("pel_alamat"))) > -1) &&
				(!request("pel_ponsel") || strrpos(strtolower($data->pel_ponsel), strtolower(request("pel_posel"))) > -1);
				
		});

    	$data = array();
        foreach($result as $r){
            $item = array();
            $item['pel_id'] = $r->pel_id;
            $item['pel_nama'] = $r->pel_nama;
            $item['pel_alamat'] = $r->pel_alamat;
            $item['pel_lahir'] = date_format(date_create($r->pel_lahir),"d/m/Y");
            $item['pel_kota'] = $r->pel_kota;
            $item['pel_pos'] = $r->pel_pos;
            $item['pel_telp'] = $r->pel_telp;
            $item['pel_ponsel'] = $r->pel_ponsel;
            $item['pel_email'] = $r->pel_email;
            $item['pel_sales'] = $r->karyawan_nama;
            $item['detail'] = "<a href='#detail' class='green-text detail' onclick='detail(this)'  data-id='".$r->pel_id."'  title='Detail'><span class='material-icons'  style='font-size:20px'>search</span></a>";
            array_push($data, $item);
        }

    	return json_encode($data);
    }

    function destroy(){
		return Pelanggan::where('pel_id', request("pel_id"))->delete();
    }

     function detail($id){
        $data['pemesan'] = DB::table('tb_pelanggan')->where("pel_id",$id)->first();
        $data['spk'] =DB::table('tb_spk')
            ->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
            ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
            ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
            ->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
            ->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
            ->leftjoin('tb_leasing', 'tb_spk.spk_leasing', '=', 'tb_leasing.leasing_id')
            ->where("spk_pel_id",$id)
            ->get();


        return json_encode($data);
    }
}
