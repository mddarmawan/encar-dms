<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiAksesorisController extends Controller
{
   function index(){
        $data = DB::table('tb_aksesoris')
                -> get();
        $result = $data->filter(function ($data) {
            return 
                (!request("aksesoris_kode") || strrpos(strtolower($data->aksesoris_kode), strtolower(request("aksesoris_kode"))) > -1) && 
                (!request("aksesoris_nama") || strrpos(strtolower($data->aksesoris_nama), strtolower(request("aksesoris_nama"))) > -1)&& 
                (!request("aksesoris_kendaraan") || strrpos(strtolower($data->aksesoris_kendaraan), strtolower(request("aksesoris_kendaraan"))) > -1);
        });
          $data = array();
        foreach($result as $r){
            $item = array();
            $item['aksesoris_id'] = $r->aksesoris_id;
            $item['aksesoris_kode'] = $r->aksesoris_kode;
            $item['aksesoris_nama'] = $r->aksesoris_nama;
            $item['aksesoris_kendaraan'] = $r->aksesoris_kendaraan;
            $item['aksesoris_harga'] = $r->aksesoris_harga;
            $item['aksesoris_vendor'] = $r->aksesoris_vendor;
            $item['aksesoris_status'] = $r->aksesoris_status;
            array_push($data, $item);
        }

        return json_encode($data);
   }

   function store(){
        $this->validate(request(), [
            "aksesoris_kode"      => "required",
            "aksesoris_nama"      => "required",
            "aksesoris_vendor"      => "required",
            "aksesoris_harga"     => "required"        
            ]);

       $insert = array(
            "aksesoris_kode"     =>  request("aksesoris_kode"),
            "aksesoris_nama"     =>  request("aksesoris_nama"),
            "aksesoris_kendaraan"    =>  request("aksesoris_kendaraan"),
            "aksesoris_harga"    =>  request("aksesoris_harga"),
            "aksesoris_vendor"    =>  request("aksesoris_vendor")
        );

       $id = DB::table('tb_aksesoris')->insertGetId($insert, 'aksesoris_id');

        return json_encode(DB::table('tb_aksesoris')->where('aksesoris_id', $id)->first());
    }

   

    function update(){
        $this->validate(request(), [
            "aksesoris_kode"      => "required",
            "aksesoris_nama"      => "required",
            "aksesoris_vendor"      => "required",
            "aksesoris_harga"     => "required"
        ]);

          DB::table('tb_aksesoris')
                    ->where("aksesoris_id",request("aksesoris_id"))
                    -> update([
                            "aksesoris_kode"     =>  request("aksesoris_kode"),
                            "aksesoris_nama"     =>  request("aksesoris_nama"),
                            "aksesoris_kendaraan"     =>  request("aksesoris_kendaraan"),
                            "aksesoris_harga"    =>  request("aksesoris_harga"),
                            "aksesoris_vendor"    =>  request("aksesoris_vendor")
                    ]);

                   return json_encode(DB::table('tb_aksesoris')->where("aksesoris_id",request("aksesoris_id"))->first());
    }

    function destroy(){
        return DB::table('tb_aksesoris')->where('aksesoris_id', request("aksesoris_id"))->delete();
    }

 
}
