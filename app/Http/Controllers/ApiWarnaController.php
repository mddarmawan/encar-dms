<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

use App\Warna;
class ApiWarnaController extends Controller
{
 	function index(){
    	$data = DB::table('tb_warna') 
                 ->leftjoin('tb_type', 'tb_warna.warna_type', '=', 'tb_type.type_id')
                -> where("warna_id",">",0)->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1)&& 
                (!request("warna_type") || strrpos(strtolower($data->warna_type), strtolower(request("warna_type"))) > -1);
		});

    	return json_encode($result);
    }

    function warna_variant(){
      $data =DB::table('tb_warna')->get();
        $result = $data->filter(function ($data) {
            return 
                (!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1);
        });

        return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "warna_nama"      => "required",
            "warna_type"      => "required",
        ]);

	    $insert= array (
	        "warna_nama"     		=>  request("warna_nama"),
            "warna_type"            =>  request("warna_type")
	    );

        $id = DB::table('tb_warna')-> insertGetId($insert, 'warna_id');
        return json_encode(DB::table('tb_warna')-> where("warna_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "warna_nama"      => "required"
        ]);

	    DB::table('tb_warna')-> where("warna_id",request("warna_id"))->update([
	        "warna_nama"     		=>  request("warna_nama")
            ]);

	    return json_encode(DB::table('tb_warna')-> where("warna_id",request("warna_id"))->first());
    }

    function destroy(){
		return DB::table('tb_warna')-> where('warna_id', request("warna_id"))->delete();
    }    
}
