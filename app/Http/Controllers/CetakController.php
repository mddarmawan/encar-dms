<?php

namespace App\Http\Controllers;

require_once('mpdf/vendor/autoload.php');

use Illuminate\Http\Request;
use Response;
use mPDF;
use App\Http\Controllers\ApiLaporanController;

class CetakController extends Controller
{
	public function index(){
		$mpdf = new mPDF();

		//load the view and saved it into $html variable
        $html = view('modules.cetak.f_kendaraan');
 
        //PDF filename that user will get to download
        $pdfFilePath = "Invoice RentalMoCar.pdf";
 
       //generate the PDF from html
        $mpdf->WriteHTML($html);
		$mpdf->debug = true;
 
        //download PDF
		$mpdf->Output($pdfFilePath, "D");
	}

	public function cetak($aksi, $id){
	    /**
	     * Create a new mPDF instance.
	     */
		$mpdf = new mPDF();

	    /**
	     * Load the required data.
	     */
	    $data = json_decode((new ApiLaporanController)->$aksi($id));
		$data = $data[0];

	    /**
	     * load the view and saved it into $html variable.
	     */
        $html = view('modules.cetak.'.$aksi, compact('data'));
 
	    /**
	     * PDF filename that user will get to download.
	     */
        $pdfFilePath = $aksi.'_'.date('h:i:s').'.pdf';
 
	    /**
	     * Generate the PDF from html.
	     */
        $mpdf->WriteHTML($html);
 
	    /**
	     * Download the PDF file.
	     */
		$mpdf->Output($pdfFilePath, "D");
	}
}