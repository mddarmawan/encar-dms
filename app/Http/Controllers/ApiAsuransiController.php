<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Asuransi;
use App\JenAsuransi;

class ApiAsuransiController extends Controller
{
    function asuransi_read(){
    	$data = DB::table('tb_asuransi')
                ->where("asuransi_status","!=",0)
                ->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("asuransi_nama") || strrpos(strtolower($data->asuransi_nama), strtolower(request("asuransi_nama"))) > -1) &&
                (!request("asuransi_telp") || strrpos(strtolower($data->asuransi_telp), strtolower(request("asuransi_telp"))) > -1) &&
                (!request("asuransi_email") || strrpos(strtolower($data->asuransi_email), strtolower(request("asuransi_email"))) > -1) &&
                (!request("asuransi_alamat") || strrpos(strtolower($data->asuransi_alamat), strtolower(request("asuransi_alamat"))) > -1) &&
                (!request("asuransi_keterangan") || strrpos(strtolower($data->asuransi_keterangan), strtolower(request("asuransi_keterangan"))) > -1);
		});

    	return json_encode($result);
    }

    function add_asuransi(){
    	$this->validate(request(), [
            "asuransi_nama"     	=> "required",
        ]);

        
        $insert = array(
            "asuransi_nama"     	  =>  request("asuransi_nama"),
            "asuransi_telp"           =>  request("asuransi_telp"),
            "asuransi_email"          =>  request("asuransi_email"),
            "asuransi_alamat"         =>  request("asuransi_alamat"),
            "asuransi_keterangan"     =>  request("asuransi_keterangan")
        );

        $id = DB::table('tb_asuransi')-> insertGetId($insert,'asuransi_id');
        return json_encode(DB::table('tb_asuransi')->where("asuransi_id", $id)->first());
    }

    function update_asuransi(){
    	$this->validate(request(), [
            "asuransi_nama"     	=> "required",
        ]);

	   DB::table('tb_asuransi')->where("asuransi_id",request("asuransi_id"))->update([
	       "asuransi_nama"     	      =>  request("asuransi_nama"),
            "asuransi_telp"           =>  request("asuransi_telp"),
            "asuransi_email"          =>  request("asuransi_email"),
            "asuransi_alamat"         =>  request("asuransi_alamat"),
            "asuransi_keterangan"     =>  request("asuransi_keterangan")
	    ]);

        return json_encode(DB::table('tb_asuransi')->where("asuransi_id",request("asuransi_id"))->first());
    }

    function destroy_asuransi(){
          DB::table('tb_asuransi')->where("asuransi_id",request("asuransi_id"))->update(["asuransi_status"     =>  0 ]);

        return json_encode(DB::table('tb_asuransi')-> where("asuransi_id",request("asuransi_id"))->first()); 
		//return Asuransi::where('asuransi_id', request("asuransi_id"))->delete();
    }

}
