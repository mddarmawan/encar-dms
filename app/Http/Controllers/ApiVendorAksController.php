<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\VendorAks;

class ApiVendorAksController extends Controller
{
    function index(){
    	$data = DB::table('tb_vendorAks')-> where('vendorAks_status','!=',0)->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("vendorAks_nama") || strrpos(strtolower($data->vendorAks_nama), strtolower(request("vendorAks_nama"))) > -1) &&
				 (!request("vendorAks_email") || strrpos(strtolower($data->vendorAks_email), strtolower(request("vendorAks_email"))) > -1) &&
				 (!request("vendorAks_telp") || strrpos(strtolower($data->vendorAks_telp), strtolower(request("vendorAks_telp"))) > -1) &&
				 (!request("vendorAks_kodepos") || strrpos(strtolower($data->vendorAks_kodepos), strtolower(request("vendorAks_kodepos"))) > -1)&&
				 (!request("vendorAks_kota") || strrpos(strtolower($data->vendorAks_kota), strtolower(request("vendorAks_kota"))) > -1);
		});

    	return json_encode($result);
    }

     function store(){
    	$this->validate(request(), [
            "vendorAks_nama"      => "required",
            "vendorAks_telp"      => "required"
        ]);

        
        $insert = array(
            "vendorAks_nama"     =>  request("vendorAks_nama"),
            "vendorAks_alamat"     =>  request("vendorAks_alamat"),
            "vendorAks_telp"           =>  request("vendorAks_telp"),
            "vendorAks_kota"           =>  request("vendorAks_kota"),
            "vendorAks_email"           =>  request("vendorAks_email"),
            "vendorAks_kodepos"          =>  request("vendorAks_kodepos")
        );
        $id= DB::table('tb_vendorAks')-> insertGetId($inser,'vendorAks_id');
        return json_encode(DB::table('tb_vendorAks')-> where("vendorAks_id",$insert->id)->first()) ;
    }

    function update(){
    	$this->validate(request(), [
            "vendorAks_nama"      => "required",
            "vendorAks_telp"      => "required"
        ]);

	    DB::table('tb_vendorAks')-> where("vendorAks_id",request("vendorAks_id"))->update([
	        "vendorAks_nama"     =>  request("vendorAks_nama"),
            "vendorAks_alamat"     =>  request("vendorAks_alamat"),
            "vendorAks_telp"           =>  request("vendorAks_telp"),
            "vendorAks_kota"           =>  request("vendorAks_kota"),
            "vendorAks_email"           =>  request("vendorAks_email"),
            "vendorAks_kodepos"          =>  request("vendorAks_kodepos")
	    ]);

	    return json_encode(DB::table('tb_vendorAks')-> where("vendorAks_id",request("vendorAks_id"))->first()) ;
    }

    function destroy(){
         DB::table('tb_vendorAks')-> where("vendorAks_id",request("vendorAks_id"))->update(["vendorAks_status"          => 0 ]);

        return VDB::table('tb_vendorAks')-> where("vendorAks_id",request("vendorAks_id"))->first();
		//return VendorAks::where('vendorAks_id', request("vendorAks_id"))->delete();
    }    
}
